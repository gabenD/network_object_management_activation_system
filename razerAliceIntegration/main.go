package main

import (
	"nomas_razer_alice_integration/pkg/configurator"
	"nomas_razer_alice_integration/pkg/cpu_fan_polling"
	"nomas_razer_alice_integration/pkg/razer_client"
	"nomas_shared/pkg/load_configurator"
	"nomas_shared/pkg/long_polling_client"
	"os"

	log "github.com/sirupsen/logrus"
)

func main() {
	err := os.Setenv("PATH", `c:\Windows\System32`)
	if err != nil {
		return
	}
	conf, err := load_configurator.LoadConfigFromArguments[configurator.RazerAliceConf]()
	if err != nil {
		log.Panic(err)
	}
	load_configurator.SetLogger(&conf.Debug)

	razerManager := razer_client.NewRazerManager(&conf.Razer, &conf.Debug)
	defer razerManager.Shutdown()

	callbacks := []func(
		*cpu_fan_polling.ModelCPUFan,
		*long_polling_client.LongPollingClient,
	){func(
		fan *cpu_fan_polling.ModelCPUFan,
		ctx *long_polling_client.LongPollingClient,
	) {
		log.Debug("callback", fan)
		razerManager.SetParams(&razer_client.RazerModel{
			Power:      fan.Power.Value == 1,
			Brightness: fan.Brightness.Value,
			Effect:     "",
			Color:      fan.Color.Hex,
		})
	}}
	cpuPolling, err := cpu_fan_polling.NewCpuFanObserver(
		&conf.IoT,
		&conf.Debug,
		&callbacks,
	)
	if err != nil {
		log.Panic(err)
	}
	defer cpuPolling.Shutdown()
	cpuPolling.RunForever()
}
