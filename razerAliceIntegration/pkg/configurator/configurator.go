package configurator

import (
	"nomas_razer_alice_integration/pkg/razer_client"
	"nomas_shared/pkg/long_polling_client"
)

type RazerAliceConf struct {
	Debug bool                     `json:"debug"`
	IoT   long_polling_client.Conf `json:"iot"`
	Razer razer_client.Conf        `json:"razer"`
}
