package razer_client

import (
	"fmt"
	"time"

	"github.com/go-resty/resty/v2"

	log "github.com/sirupsen/logrus"
)

type RazerChromaApiClient struct {
	config *Conf
	debug  *bool
	client *resty.Client
	store  RazerChromaInitStore
}

func NewRazerChromaApiClient(config *Conf, debug *bool) *RazerChromaApiClient {
	client := resty.New()
	client.SetDebug(*debug)
	client.SetBaseURL(config.BaseUrl)
	client.SetHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0")
	client.SetTimeout(1 * time.Minute)
	client.SetHeader("Accept", "application/json")
	return &RazerChromaApiClient{
		config: config,
		debug:  debug,
		client: client,
		store:  RazerChromaInitStore{},
	}
}

func (c *RazerChromaApiClient) Init() {
	if c.store.session != nil {
		return
	}
	log.Debug("(RazerChromaApiClient) init")
	const url = "/razer/chromasdk"
	var session RazerChromaInitModelResponse
	_, err := c.client.R().SetBody(RazerChromaInitModelDefault()).SetResult(&session).Post(url)
	if err != nil {
		log.Debug("(RazerChromaApiClient) error", err)
		return
	}
	c.store.session = &session
	return
}

func (c *RazerChromaApiClient) UnInit() error {
	if c.store.session == nil {
		return nil
	}
	log.Debug("(RazerChromaApiClient) UnInit")
	_, err := c.client.R().Delete(c.store.session.Uri)
	c.store.session = nil
	return err
}

func (c *RazerChromaApiClient) EffectNone() error {
	if c.store.session == nil {
		return nil
	}
	log.Debug("(RazerChromaApiClient) EffectNone")
	url := c.store.session.Uri + "/chromalink"
	_, err := c.client.R().SetBody(`{"effect":"CHROMA_NONE"}`).Put(url)
	return err
}

func (c *RazerChromaApiClient) EffectStatic(color int) error {
	if c.store.session == nil {
		return nil
	}
	log.Debug("(RazerChromaApiClient) EffectStatic", color)
	url := c.store.session.Uri + "/chromalink"
	body := fmt.Sprintf(`{"effect":"CHROMA_STATIC","param":{"color":%d}}`, color)
	_, err := c.client.R().SetBody(body).Put(url)
	return err
}
