package razer_client

type RazerManager struct {
	client     *RazerChromaApiClient
	events     chan<- *RazerModel
	stopSignal chan<- bool
}

func NewRazerManager(config *Conf, debug *bool) *RazerManager {
	channelSize := 100
	events := make(chan *RazerModel, channelSize)
	quit := make(chan bool, 1)
	subManager := subscriptionManager{
		quit:   quit,
		events: events,
		client: NewRazerChromaApiClient(config, debug),
	}
	go subManager.run()
	return &RazerManager{
		events:     events,
		stopSignal: quit,
	}
}

func (c *RazerManager) SetParams(params *RazerModel) {
	c.events <- params
}

func (c *RazerManager) Shutdown() {
	close(c.stopSignal)
}
