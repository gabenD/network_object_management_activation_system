package razer_client

type RazerChromaInitStore struct {
	session *RazerChromaInitModelResponse
}

func RazerChromaInitModelDefault() *RazerChromaInitModelRequest {
	return &RazerChromaInitModelRequest{
		Title:       "RazerAliceIntegration",
		Description: "...",
		Author: RazerChromaInitAuthorModel{
			Name:    "Danil Gubanov",
			Contact: "https://gubanov.site",
		},
		DeviceSupported: []string{"chromalink"},
		Category:        "application",
	}
}

type RazerChromaInitModelRequest struct {
	Title           string                     `json:"title"`
	Description     string                     `json:"description"`
	Author          RazerChromaInitAuthorModel `json:"author"`
	DeviceSupported []string                   `json:"device_supported"`
	Category        string                     `json:"category"`
}

type RazerChromaInitAuthorModel struct {
	Name    string `json:"name"`
	Contact string `json:"contact"`
}

type RazerChromaInitModelResponse struct {
	SessionId int    `json:"sessionid"`
	Uri       string `json:"uri"`
}
