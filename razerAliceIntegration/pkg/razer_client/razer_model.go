package razer_client

type RazerModel struct {
	Power      bool    `json:"power"`
	Brightness float64 `json:"brightness"  validate:"min=0,max=100"`
	Effect     string  `json:"effect"  validate:"oneof=linear static"`
	Color      string  `json:"color"`
}
