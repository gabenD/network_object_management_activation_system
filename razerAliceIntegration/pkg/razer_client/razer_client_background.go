package razer_client

import (
	"time"

	"github.com/lucasb-eyer/go-colorful"
	log "github.com/sirupsen/logrus"
)

type subscriptionManager struct {
	events <-chan *RazerModel
	quit   <-chan bool

	// Internal parameters
	client    *RazerChromaApiClient
	lastEvent *RazerModel
}

func (sm *subscriptionManager) run() {
	for {
		select {
		case event := <-sm.events:
			log.Debug("RazerClient.SubscriptionManager.run", event)
			sm.lastEvent = event
			continue
		case _ = <-sm.quit:
			break
		default:
			sm.tick()
			time.Sleep(1 * time.Second)
		}
	}
}

func (sm *subscriptionManager) tick() {
	if sm.lastEvent == nil {
		log.Debug("RazerClient.SubscriptionManager skip tick")
		return
	}
	// Constructor / Destructor
	if sm.lastEvent.Power {
		log.Debug("RazerClient.SubscriptionManager Init")
		sm.client.Init()
	} else {
		log.Debug("RazerClient.SubscriptionManager UnInit")
		_ = sm.client.UnInit()
		return
	}
	log.Debug("RazerClient.SubscriptionManager tick")
	colorModel, err := colorful.Hex(sm.lastEvent.Color)
	if err != nil {
		log.Error("RazerClient.SubscriptionManager Error: ", err)
		return
	}
	colorInt := RazerBRGColorHexToInt(colorModel, int(sm.lastEvent.Brightness))
	err = sm.client.EffectStatic(colorInt)
	if err != nil {
		log.Debug("RazerClient.SubscriptionManager EffectStatic: ", err)
		return
	}
}
