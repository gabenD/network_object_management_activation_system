package razer_client

import (
	"github.com/lucasb-eyer/go-colorful"
)

func RazerBRGColorHexToInt(color colorful.Color, brightness int) int {
	if brightness > 95 {
		brightness = 100
	}
	if brightness < 2 {
		brightness = 0
	}
	brightnessPercent := float64(brightness) / 100
	var bgr int = int(float64(int(color.B*255.0+0.5)) * brightnessPercent)
	bgr = (bgr << 8) + int(float64(int(color.G*255.0+0.5))*brightnessPercent)
	bgr = (bgr << 8) + int(float64(int(color.R*255.0+0.5))*brightnessPercent)
	return bgr
}
