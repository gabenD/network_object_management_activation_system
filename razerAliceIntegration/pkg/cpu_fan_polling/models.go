package cpu_fan_polling

type ModelCPUFan struct {
	Power       ModelCPUFanPowerValue       `json:"power"`
	Brightness  ModelCPUFanBrightnessValue  `json:"brightness"`
	Color       ModelCPUFanColorValue       `json:"color"`
	Ionization  ModelCPUFanIonizationValue  `json:"ionization"`
	Speed       ModelCPUFanSpeedValue       `json:"speed"`
	Conditioner ModelCPUFanConditionerValue `json:"conditioner"`
}

type ModelCPUFanPowerValue struct {
	Value int `json:"value"  validate:"min=0,max=1"`
}

type ModelCPUFanBrightnessValue struct {
	Value float64 `json:"value"  validate:"min=0,max=100"`
}

type ModelCPUFanColorValue struct {
	Value int64  `json:"value"`
	Red   int    `json:"r"  validate:"min=0,max=255"`
	Green int    `json:"g"  validate:"min=0,max=255"`
	Blue  int    `json:"b"  validate:"min=0,max=255"`
	Hex   string `json:"#rgb"`
}

type ModelCPUFanIonizationValue struct {
	Value int `json:"value"  validate:"min=0,max=1"`
}

type ModelCPUFanSpeedValue struct {
	Value string `json:"value"  validate:"oneof=auto low medium high"`
}

type ModelCPUFanConditionerValue struct {
	Value string `json:"value"  validate:"oneof=fan_only dry cool heat auto ecoo"`
}
