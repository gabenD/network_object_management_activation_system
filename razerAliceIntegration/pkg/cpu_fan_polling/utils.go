package cpu_fan_polling

import (
	"nomas_shared/pkg/long_polling_client"
)

func NewCpuFanObserver(
	config *long_polling_client.Conf,
	debug *bool,
	callbacks *[]func(
		*ModelCPUFan,
		*long_polling_client.LongPollingClient,
	),
) (*long_polling_client.LongPollingManager[ModelCPUFan], error) {
	confManager, confClient := long_polling_client.ParseConf(config)
	client := long_polling_client.NewLongPollingClient(
		confClient,
		debug,
	)
	return long_polling_client.StartLongPolling[ModelCPUFan](confManager, client, callbacks)
}
