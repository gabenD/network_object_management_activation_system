package filter

func Choose[T any](slice []T, keep func(*T) bool) []T {
	newSlice := make([]T, 0)
	for _, item := range slice {
		if keep(&item) {
			newSlice = append(newSlice, item)
		}
	}
	return newSlice
}

func Bool2Int(b bool) int {
	if b {
		return 1
	}
	return 0
}
