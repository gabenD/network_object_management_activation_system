package sber_client

import (
	"fmt"
	"net/http"
	"strconv"
	"time"

	log "github.com/sirupsen/logrus"

	"github.com/go-resty/resty/v2"
)

type AuthorizationSberClient struct {
	debug  *bool
	client *resty.Client
}

func (c *AuthorizationSberClient) GetCookies(expires int64) (*AuthorizationSberClientTokenResponse, error) {
	const url = "/api/v1/activation_sber/token"
	var cookies AuthorizationSberClientTokenResponse
	resp, err := c.client.R().SetQueryParam(
		"expires",
		strconv.FormatInt(expires, 10),
	).SetResult(&cookies).Post(url)
	if resp.IsError() {
		return &cookies, AuthManagerException
	}
	if err != nil {
		return &cookies, err
	}
	return &cookies, nil
}

type ReAuthManager struct {
	client *AuthorizationSberClient
	store  *AuthorizationSberClientStore
}

func (c *ReAuthManager) backgroundTick() time.Duration {
	tokenData, err := c.client.GetCookies(time.Now().Unix() + 7*60)
	if err != nil {
		log.Error("ReAuthManager error: ", err)
		return 1 * time.Minute
	}
	// За 7 минут до уничтожения токена пойдём и перезапросим его
	// Если backend присылает старый токен, через минутку попытаемся его перезапрсить
	seconds := tokenData.Expires - time.Now().Unix() - 7*60
	if seconds <= 0 {
		seconds = 60
	}
	c.store.Lock()
	defer c.store.Unlock()
	c.store.lastTokenResponse = tokenData
	c.store.needChangeCookies = true
	return time.Duration(seconds) * time.Second
}

func (c *ReAuthManager) background() {
	for {
		time.Sleep(c.backgroundTick())
	}
}

func (c *ReAuthManager) SetCookies(client *resty.Client) bool {
	c.store.Lock()
	defer c.store.Unlock()
	if !c.store.needChangeCookies {
		return false
	}
	client.Cookies = []*http.Cookie{}
	if c.store.lastTokenResponse == nil {
		return false
	}
	c.store.needChangeCookies = false

	for _, cookieToken := range c.store.lastTokenResponse.Cookies {
		client.SetCookie(&http.Cookie{
			Domain: "activation.sber.ru",
			Name:   cookieToken.Name,
			Value:  cookieToken.Value,
		})
	}
	return true
}

func NewReAuthManager(config *AuthorizationClientConf, debug *bool) *ReAuthManager {
	client := resty.New()
	client.SetDebug(*debug)
	client.SetTimeout(10 * time.Minute)
	client.SetBaseURL(config.BaseUrl)
	client.SetHeader("Accept", "application/json")
	client.SetHeader("Content-Type", "application/json")
	client.SetHeader("Authorization", fmt.Sprintf("Bearer %s", config.Token))

	authClient := AuthorizationSberClient{
		debug:  debug,
		client: client,
	}
	reAuthManager := ReAuthManager{
		client: &authClient,
		store:  &AuthorizationSberClientStore{},
	}
	go reAuthManager.background()
	return &reAuthManager
}
