package sber_client

import "sync"

type AuthorizationSberClientCookie struct {
	Name    string `json:"name"`
	Domain  string `json:"domain"`
	Path    string `json:"path"`
	Value   string `json:"value"`
	Expires int64  `json:"expires"`
}

type AuthorizationSberClientTokenResponse struct {
	Cookies []AuthorizationSberClientCookie `json:"cookies"`
	Expires int64                           `json:"expires"`
}

type AuthorizationSberClientStore struct {
	sync.Mutex        // ← этот мьютекс защищает данные ниже
	lastTokenResponse *AuthorizationSberClientTokenResponse
	needChangeCookies bool
}
