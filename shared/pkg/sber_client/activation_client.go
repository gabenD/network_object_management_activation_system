package sber_client

import (
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"

	"github.com/go-resty/resty/v2"
)

type ActivationSberClient struct {
	debug         *bool
	reAuthManager *ReAuthManager
	client        *resty.Client
}

func NewActivationSberClient(conf *AuthorizationClientConf, debug *bool) *ActivationSberClient {
	client := resty.New()
	client.SetDebug(*debug)
	client.SetTimeout(1 * time.Minute)
	client.SetHeader("Accept", "application/json")
	client.SetHeader("Content-Type", "application/json")
	reAuthManager := NewReAuthManager(conf, debug)
	return &ActivationSberClient{
		debug:         debug,
		reAuthManager: reAuthManager,
		client:        client,
	}
}

func (c *ActivationSberClient) exchange() {
	const url = "https://activation.sber.ru/api/ihapi/tokens/exchange"
	resp, err := c.client.R().SetBody(`{"scope":["smarthome.api"],"requestedTokenType":"access_token","subjectTokenType":"access_token","subjectIssuer":"ihapi","audience":"smarthome"}`).Post(url)
	if resp.IsError() {
		log.Error("ActivationSberClient.exchange response is error. Status: ", resp.StatusCode())
		return
	}
	if err != nil {
		log.Error("ActivationSberClient.exchange Error: ", err)
	}
}

func (c *ActivationSberClient) reAuth() {
	if c.reAuthManager.SetCookies(c.client) {
		c.exchange()
	}
}

func (c *ActivationSberClient) SetState(deviceId string, payload *map[string]interface{}) error {
	c.reAuth()
	url := fmt.Sprintf("https://activation.sber.ru:8080/gateway/v1/devices/%s/state", deviceId)
	request := GatewayStatePutRequest{
		DesiredState: []*map[string]interface{}{payload},
		DeviceId:     deviceId,
		Timestamp:    time.Now().UTC().Format("2006-01-02T15:04:05.999Z"),
	}
	resp, err := c.client.R().SetBody(&request).Put(url)
	if resp.IsError() {
		log.Error("ActivationSberClient.SetState response is error. Status: ", resp.StatusCode())
		return ActivationException
	}
	if err != nil {
		log.Error("ActivationSberClient.SetState Error: ", err)
		return err
	}
	return nil
}

func (c *ActivationSberClient) UpdateScenarioOnSaluteDevice(
	accountId string,
	scenarioId string,
	deviceId string,
	command string,
) error {
	c.reAuth()
	nameSaluteScenario := "UpdateScenarioOnSaluteDevice"
	url := fmt.Sprintf("https://activation.sber.ru:8080/gateway/v1/scenario/v2/scenario/%s", scenarioId)
	payload := map[string]interface{}{
		"id":         scenarioId,
		"account_id": accountId,
		"name":       nameSaluteScenario,
		"steps": []interface{}{
			map[string]interface{}{
				"tasks": []interface{}{
					map[string]interface{}{
						"type":                      "HEAD_DIALOG_COMMAND",
						"duration":                  "0s",
						"device_command_data":       nil,
						"device_group_command_data": nil,
						"home_assistant_task_data":  nil,
						"head_dialog_command_task_data": map[string]interface{}{
							"device_id": deviceId,
							"text":      command,
						},
						"Id":                            "623564",
						"regime_condition":              nil,
						"regime_command_data":           nil,
						"forced_regime_command_data":    nil,
						"start_delay":                   "0h0m0s",
						"steps":                         []interface{}{},
						"scenario_set_active_task_data": nil,
						"send_sms_command_task_data":    nil,
						"start_step_task_data":          nil,
					},
				},
				"condition": map[string]interface{}{
					"type":         "CONDITIONS",
					"time_data":    nil,
					"phrases_data": nil,
					"nested_conditions_data": map[string]interface{}{
						"conditions": []interface{}{
							map[string]interface{}{
								"type":      "PHRASES",
								"time_data": nil,
								"phrases_data": map[string]interface{}{
									"phrases": []interface{}{
										nameSaluteScenario,
									},
								},
								"nested_conditions_data": nil,
								"device_data":            nil,
							},
						},
						"relation": "OR",
					},
					"device_data": nil,
				},
			},
		},
		"image":    "https://img.iot.sberdevices.ru/p/q100/e7/a4/e715a4ce20e06797be5743f2f489e5441630170f214118d86297a6ac818d018a",
		"timezone": "Asia/Yekaterinburg",
	}
	resp, err := c.client.R().SetBody(&payload).Put(url)
	if resp.IsError() {
		log.Error(
			"ActivationSberClient.UpdateScenarioOnSaluteDevice response is error. Status: ",
			resp.StatusCode(),
		)
		return ActivationException
	}
	if err != nil {
		log.Error(
			"ActivationSberClient.UpdateScenarioOnSaluteDevice Error: ",
			err,
		)
		return err
	}
	return nil
}

func (c *ActivationSberClient) RunScenario(
	scenarioId string,
) error {
	c.reAuth()
	url := fmt.Sprintf("https://activation.sber.ru:8080/gateway/v1/scenario/v2/scenario/%s/run", scenarioId)
	payload := map[string]interface{}{
		"id": scenarioId,
	}
	resp, err := c.client.R().SetBody(&payload).Post(url)
	if resp.IsError() {
		log.Error(
			"ActivationSberClient.RunScenario response is error. Status: ",
			resp.StatusCode(),
		)
		return ActivationException
	}
	if err != nil {
		log.Error(
			"ActivationSberClient.RunScenario Error: ",
			err,
		)
		return err
	}
	return nil
}
