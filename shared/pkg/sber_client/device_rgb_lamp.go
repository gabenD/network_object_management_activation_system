package sber_client

type RGBLamp struct {
	deviceId string
	client   *ActivationSberClient
}

func NewRGBLamp(deviceId string, client *ActivationSberClient) *RGBLamp {
	return &RGBLamp{
		deviceId: deviceId,
		client:   client,
	}
}

func (c *RGBLamp) On() error {
	payload := map[string]interface{}{"bool_value": true, "key": "on_off", "type": "BOOL"}
	return c.client.SetState(c.deviceId, &payload)
}

func (c *RGBLamp) Off() error {
	payload := map[string]interface{}{"bool_value": false, "key": "on_off", "type": "BOOL"}
	return c.client.SetState(c.deviceId, &payload)
}

// Color Hsv set the Hue [0..360], Saturation and Value [0..1000] of the color.
func (c *RGBLamp) Color(h int64, s int64, v int64) error {
	payload := map[string]interface{}{
		"enum_value": "colour",
		"key":        "light_mode",
		"type":       "ENUM",
	}
	_ = c.client.SetState(c.deviceId, &payload)
	payload = map[string]interface{}{
		"color_value": map[string]interface{}{
			"h": h,
			"s": s,
			"v": v,
		},
		"key":  "light_colour",
		"type": "COLOR",
	}
	return c.client.SetState(c.deviceId, &payload)
}

// Temperature set the Temperature [0..1000], Brightness [50..1000]
func (c *RGBLamp) Temperature(temperature int64, brightness int64) error {
	payload := map[string]interface{}{
		"enum_value": "white",
		"key":        "light_mode",
		"type":       "ENUM",
	}
	_ = c.client.SetState(c.deviceId, &payload)
	payload = map[string]interface{}{
		"integer_value": brightness,
		"key":           "light_brightness",
		"type":          "INTEGER",
	}
	_ = c.client.SetState(c.deviceId, &payload)
	payload = map[string]interface{}{
		"integer_value": temperature,
		"key":           "light_colour_temp",
		"type":          "INTEGER",
	}
	return c.client.SetState(c.deviceId, &payload)
}
