package sber_client

type AuthorizationClientConf struct {
	BaseUrl string `json:"url"`
	Token   string `json:"token"`
}
