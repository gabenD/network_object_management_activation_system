package sber_client

import "fmt"

type TVSalute struct {
	deviceId   string
	scenarioId string
	accountId  string
	client     *ActivationSberClient
}

func NewTVSalute(accountId string, scenarioId string, deviceId string, client *ActivationSberClient) *TVSalute {
	return &TVSalute{
		deviceId:   deviceId,
		scenarioId: scenarioId,
		accountId:  accountId,
		client:     client,
	}
}

func (c *TVSalute) On() error {
	err := c.client.UpdateScenarioOnSaluteDevice(c.accountId, c.scenarioId, c.deviceId, "Включи телевизор")
	if err != nil {
		return err
	}
	return c.client.RunScenario(c.scenarioId)
}

func (c *TVSalute) Off() error {
	err := c.client.UpdateScenarioOnSaluteDevice(c.accountId, c.scenarioId, c.deviceId, "Выключи телевизор")
	if err != nil {
		return err
	}
	return c.client.RunScenario(c.scenarioId)
}

func (c *TVSalute) Mute() error {
	err := c.client.UpdateScenarioOnSaluteDevice(
		c.accountId,
		c.scenarioId,
		c.deviceId,
		"Выключи звук в телевизоре",
	)
	if err != nil {
		return err
	}
	return c.client.RunScenario(c.scenarioId)
}

// SetVolume set the volume [0..10]
func (c *TVSalute) SetVolume(volume int) error {
	command := fmt.Sprintf("Установи громкость %d", volume)
	err := c.client.UpdateScenarioOnSaluteDevice(c.accountId, c.scenarioId, c.deviceId, command)
	if err != nil {
		return err
	}
	return c.client.RunScenario(c.scenarioId)
}

func (c *TVSalute) ExecuteCommand(command string) error {
	err := c.client.UpdateScenarioOnSaluteDevice(c.accountId, c.scenarioId, c.deviceId, command)
	if err != nil {
		return err
	}
	return c.client.RunScenario(c.scenarioId)
}
