package sber_client

import (
	"errors"
)

var AuthManagerException = errors.New("internal error from service AuthManager")
var ActivationException = errors.New("internal error from service ActivationClient")
