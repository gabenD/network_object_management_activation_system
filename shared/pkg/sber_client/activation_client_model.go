package sber_client

type GatewayStatePutRequest struct {
	DesiredState []*map[string]interface{} `json:"desired_state"`
	DeviceId     string                    `json:"device_id"`
	Timestamp    string                    `json:"timestamp"`
}
