package sber_client

type IActivationSberClient interface {
	SetState(deviceId string, payload *map[string]interface{}) error
	UpdateScenarioOnSaluteDevice(
		accountId string,
		scenarioId string,
		deviceId string,
		command string,
	) error
	RunScenario(
		scenarioId string,
	) error
}
