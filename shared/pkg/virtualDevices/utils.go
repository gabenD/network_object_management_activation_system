package virtualDevices

func BrightnessMap(value int, fromStart int, fromEnd int, toStart int, toEnd int) int {
	// Проверка на деление на ноль
	if fromEnd == fromStart {
		return toStart
	}
	// Преобразование значения из одного диапазона в другой
	return (value-fromStart)*(toEnd-toStart)/(fromEnd-fromStart) + toStart
}
