package metabase_sync_client

type INewMetabaseSyncClient interface {
	IncomeExpense(atDate string, toDate string) (*MetabaseSyncClientResponse, error)
	CurrencyLimit() (*MetabaseSyncClientResponse, error)
}
