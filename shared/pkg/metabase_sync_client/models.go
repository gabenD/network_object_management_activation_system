package metabase_sync_client

type MetabaseSyncClientResponse struct {
	Message string `json:"message"`
}
