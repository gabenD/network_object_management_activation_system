package metabase_sync_client

import (
	"errors"
)

var MetabaseClientException = errors.New("Internal error from service MetabaseSyncClient")
