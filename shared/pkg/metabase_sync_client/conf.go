package metabase_sync_client

type MetabaseSyncClientConf struct {
	BaseUrl           string `json:"url"`
	Token             string `json:"token"`
	MaxTimeoutSeconds int64  `json:"max_timeout_seconds"`
}
