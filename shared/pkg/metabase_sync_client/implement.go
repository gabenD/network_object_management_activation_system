package metabase_sync_client

import (
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"

	"github.com/go-resty/resty/v2"
)

type MetabaseSyncClient struct {
	debug  *bool
	client *resty.Client
}

func NewMetabaseSyncClient(conf *MetabaseSyncClientConf, debug *bool) *MetabaseSyncClient {
	client := resty.New()
	client.SetDebug(*debug)
	client.SetBaseURL(conf.BaseUrl)
	client.SetTimeout(time.Duration(conf.MaxTimeoutSeconds) * time.Second)
	client.SetHeader("Accept", "application/json")
	client.SetHeader("Content-Type", "application/json")
	client.SetHeader("Authorization", fmt.Sprintf("Bearer %s", conf.Token))
	return &MetabaseSyncClient{
		debug:  debug,
		client: client,
	}
}

func (c *MetabaseSyncClient) IncomeExpense(atDate string, toDate string) (*MetabaseSyncClientResponse, error) {
	url := "/sync/notion"
	request := map[string]string{
		"at": atDate,
		"to": toDate,
	}
	var result MetabaseSyncClientResponse
	resp, err := c.client.R().SetBody(&request).SetResult(&result).Post(url)
	if resp.IsError() {
		log.Error("MetabaseSyncClient.IncomeExpense response is error. Status: ", resp.StatusCode())
		return &result, MetabaseClientException
	}
	if err != nil {
		log.Error("MetabaseSyncClient.IncomeExpense Error: ", err)
	}
	return &result, err
}

func (c *MetabaseSyncClient) CurrencyLimit() (*MetabaseSyncClientResponse, error) {
	url := "/sync/notion/limit"
	request := map[string]string{}
	var result MetabaseSyncClientResponse
	resp, err := c.client.R().SetBody(&request).SetResult(&result).Post(url)
	if resp.IsError() {
		log.Error("MetabaseSyncClient.CurrencyLimit response is error. Status: ", resp.StatusCode())
		return &result, MetabaseClientException
	}
	if err != nil {
		log.Error("MetabaseSyncClient.CurrencyLimit Error: ", err)
	}
	return &result, nil
}
