package gyverTwinkClient

import (
	"fmt"
	"net"

	log "github.com/sirupsen/logrus"
)

const (
	GyverTwinkEffectPartyGrad     = 0   // Разноцветные переливания быстрые
	GyverTwinkEffectRaibowGrad    = 1   // Радужные медленные переливания
	GyverTwinkEffectStripeGrad    = 2   // Очень быстрое мерцание
	GyverTwinkEffectSunsetGrad    = 4   // Попеременное перемещение оранжевых пикселей на синем
	GyverTwinkEffectPepsiGrad     = 4   // Из синего в красный перемещение
	GyverTwinkEffectWarmGrad      = 5   // Переключение от красных оттенков к более белым
	GyverTwinkEffectColdGrad      = 6   // Переключение от синих оттенков к более белым
	GyverTwinkEffectHotGrad       = 7   // Переключение от красных оттенков к зеленым и более белым
	GyverTwinkEffectPinkGrad      = 8   // Переключение от розового к синим
	GyverTwinkEffectCyberGrad     = 9   // Переключение фиолетовый
	GyverTwinkEffectRedWhiteGrad  = 10  // Переключение красные/белые
	GyverTwinkEffectPartyNoise    = 11  // Шумное перемещение красный, зеленый, синий
	GyverTwinkEffectRainbowNoise  = 12  // Шумное перемещение все цвета
	GyverTwinkEffectStripeNoise   = 13  // Мигание синим
	GyverTwinkEffectSunsetNoise   = 14  // Мигание оранжевых пикселей на синем
	GyverTwinkEffectPepsiNoise    = 15  // Мигание из синего в красный
	GyverTwinkEffectWarmNoise     = 16  // Мигание от красных оттенков к более белым
	GyverTwinkEffectColdNoise     = 17  // Мигание от синих оттенков к более белым
	GyverTwinkEffectHotNoise      = 18  // Мигание от красных оттенков к зеленым
	GyverTwinkEffectPinkNoise     = 19  // Мигание от розового к синим
	GyverTwinkEffectCyberNoise    = 20  // Мигание фиолетовый
	GyverTwinkEffectRedWhiteNoise = 21  // Мигание красные/белые
	GyverTwinkEffectNext          = 255 // Мигание красные/белые
)

type GyverTwinkClient struct {
}

func NewGyverTwinkClient() *GyverTwinkClient {
	return &GyverTwinkClient{}
}

func (d *GyverTwinkClient) request(ipAddress string, request []byte) error {
	udpAddr, err := net.ResolveUDPAddr("udp", ipAddress)
	if err != nil {
		log.Debug(fmt.Sprintf("Error resolving UDP address: %s", ipAddress))
		return err
	}
	// Dial to the address with UDP
	conn, err := net.DialUDP("udp", nil, udpAddr)
	defer conn.Close()
	if err != nil {
		log.Debug(fmt.Sprintf("Error connecting to server: %s", ipAddress))
		return err
	}
	_, err = conn.Write(request)
	log.Debug(fmt.Sprintf("Sended request to server: %s", ipAddress))
	return err
}

func (d *GyverTwinkClient) PowerOn(ipAddress string) error {
	return d.request(ipAddress, []byte{'G', 'T', 2, 1, 255})

}

func (d *GyverTwinkClient) PowerOff(ipAddress string) error {
	return d.request(ipAddress, []byte{'G', 'T', 2, 1, 0})
}

func (d *GyverTwinkClient) SetBrightness(ipAddress string, brightness uint8) error {
	return d.request(ipAddress, []byte{'G', 'T', 2, 2, brightness})
}

func (d *GyverTwinkClient) NextEffect(ipAddress string) error {
	return d.request(ipAddress, []byte{'G', 'T', 2, 6})
}

func (d *GyverTwinkClient) SetEffect(ipAddress string, effect uint8) error {
	if effect == GyverTwinkEffectNext {
		return d.NextEffect(ipAddress)
	}
	return d.request(ipAddress, []byte{'G', 'T', 4, 0, effect})
}
