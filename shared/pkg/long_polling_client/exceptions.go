package long_polling_client

import (
	"errors"
)

var ClientLongPollingException = errors.New("Internal error from service LongPollingClient")
