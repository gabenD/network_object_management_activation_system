package long_polling_client

import (
	"strconv"
	"time"

	"github.com/go-resty/resty/v2"

	log "github.com/sirupsen/logrus"
)

type LongPollingClient struct {
	debug  *bool
	client *resty.Client
}

func NewLongPollingClient(config *LongPollingClientConf, debug *bool) *LongPollingClient {
	client := resty.New()
	client.SetDebug(*debug)
	client.SetTimeout(time.Duration(config.MaxTimeoutSeconds) * time.Second)
	client.SetBaseURL(config.BaseUrl)
	client.SetHeader("Accept", "application/json")
	return &LongPollingClient{
		debug:  debug,
		client: client,
	}
}

func GetStore[R any](c *LongPollingClient, deviceId string) (*R, error) {
	const url = "/devices"
	var respModel R
	response, err := c.client.R().SetBody(DeviceStoreGetRequest{
		Device: deviceId,
		Action: "store",
	}).SetResult(&respModel).Post(url)
	if response.IsError() {
		return &respModel, ClientLongPollingException
	}
	return &respModel, err
}

func SetStore[R any](c *LongPollingClient, deviceId string, token string, params *R) error {
	const url = "/devices"
	response, err := c.client.R().SetBody(DeviceStoreSetRequest[R]{
		Token:  token,
		Device: deviceId,
		Params: *params,
	}).Post(url)
	if response.IsError() {
		return ClientLongPollingException
	}
	return err
}

func GetLongPollingAllEvents[R any](
	c *LongPollingClient,
	deviceId string,
	sinceTime int64,
) (*DeviceEventsResponse[R], error) {
	url := "/longPolling/" + deviceId
	var respModel DeviceEventsResponse[R]
	response, err := c.client.R().SetQueryParam(
		"since_time",
		strconv.FormatInt(sinceTime, 10),
	).SetResult(&respModel).Get(url)
	if err != nil {
		return &respModel, err
	}
	if response.IsError() {
		return &respModel, ClientLongPollingException
	}
	return &respModel, err
}

func GetLongPollingStore[R any](
	c *LongPollingClient,
	deviceId string,
	sinceTime int64,
) (*DeviceEventsResponseLast[R], error) {
	responseEvents, err := GetLongPollingAllEvents[R](c, deviceId, sinceTime)
	if err != nil {
		return nil, err
	}
	var maxTimestamp int64 = 0
	respModel := DeviceEventsResponseLast[R]{
		Timeout:   responseEvents.Timeout,
		Timestamp: responseEvents.Timestamp,
	}
	for _, value := range responseEvents.Events {
		if maxTimestamp <= value.Timestamp {
			maxTimestamp = value.Timestamp
			respModel = DeviceEventsResponseLast[R]{
				Timeout:   responseEvents.Timeout,
				Timestamp: value.Timestamp,
				Category:  value.Category,
				Data:      value.Data,
			}
			log.Debug("LongPollingClient.GetLongPollingStore responseEvents<-value.Timestamp:", value.Timestamp)
		}
	}
	log.Debug("LongPollingClient.GetLongPollingStore respModel:", respModel)
	return &respModel, nil
}
