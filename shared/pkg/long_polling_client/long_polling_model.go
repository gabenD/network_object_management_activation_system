package long_polling_client

type DeviceStoreGetRequest struct {
	Device string `json:"device"`
	Action string `json:"action"`
}

type DeviceStoreSetRequest[R any] struct {
	Token  string `json:"token"`
	Device string `json:"device"`
	Params R      `json:"params"`
}

type DeviceEventsResponseItem[R any] struct {
	Timestamp int64  `json:"timestamp"`
	Category  string `json:"category"`
	Data      R      `json:"data"`
}

type DeviceEventsResponse[R any] struct {
	Timeout   string                        `json:"timeout"`
	Timestamp int64                         `json:"timestamp"`
	Events    []DeviceEventsResponseItem[R] `json:"events"`
}

type DeviceEventsResponseLast[R any] struct {
	Timeout   string `json:"timeout"`
	Timestamp int64  `json:"timestamp"`
	Category  string `json:"category"`
	Data      R      `json:"data"`
}
