package long_polling_client

type Conf struct {
	BaseUrl              string `json:"url"`
	MaxTimeoutSeconds    int64  `json:"max_timeout_seconds"`
	DeviceId             string `json:"device_id"`
	FirstStateFromServer bool   `json:"first_state_from_server"`
	Token                string `json:"token"`
}

func ParseConf(conf *Conf) (*LongPollingManagerConf, *LongPollingClientConf) {
	return &LongPollingManagerConf{
			DeviceId:             conf.DeviceId,
			FirstStateFromServer: conf.FirstStateFromServer,
			Token:                conf.Token,
		}, &LongPollingClientConf{
			BaseUrl:           conf.BaseUrl,
			MaxTimeoutSeconds: conf.MaxTimeoutSeconds,
		}
}

type LongPollingManagerConf struct {
	DeviceId             string `json:"device_id"`
	FirstStateFromServer bool   `json:"first_state_from_server"`
	Token                string `json:"token"`
}

type LongPollingClientConf struct {
	BaseUrl           string `json:"url"`
	MaxTimeoutSeconds int64  `json:"max_timeout_seconds"`
}
