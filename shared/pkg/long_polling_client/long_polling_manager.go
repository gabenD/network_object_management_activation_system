package long_polling_client

import (
	"nomas_shared/pkg/delayer"
	"time"

	log "github.com/sirupsen/logrus"
)

type LongPollingManager[R any] struct {
	conf                  *LongPollingManagerConf
	client                *LongPollingClient
	Events                chan<- R
	stopSignal            chan<- bool
	loopManagerInitClient bool
	skipFirstState        bool
	loopManagerSinceTime  int64
	delayer               *delayer.Delayer
}

type subscriptionManager[R any] struct {
	client    *LongPollingClient
	Callbacks *[]func(*R, *LongPollingClient)
	Events    <-chan R
	Quit      <-chan bool
}

func (sm *subscriptionManager[R]) run() {
	for {
		select {
		case event := <-sm.Events:
			log.Println("New store event", event)
			for _, callback := range *sm.Callbacks {
				callback(&event, sm.client)
			}
		case _ = <-sm.Quit:
			break
		}
	}
}

func StartLongPolling[R any](
	conf *LongPollingManagerConf,
	client *LongPollingClient,
	callbacks *[]func(*R, *LongPollingClient),
) (*LongPollingManager[R], error) {
	channelSize := 100
	events := make(chan R, channelSize)
	quit := make(chan bool, 1)
	subManager := subscriptionManager[R]{
		client:    client,
		Callbacks: callbacks,
		Quit:      quit,
		Events:    events,
	}
	go subManager.run()
	manager := LongPollingManager[R]{
		conf:                  conf,
		client:                client,
		Events:                events,
		stopSignal:            quit,
		loopManagerInitClient: false,
		skipFirstState:        !conf.FirstStateFromServer,
		loopManagerSinceTime:  0,
		delayer:               delayer.NewDelayer(),
	}
	return &manager, nil
}

func (c *LongPollingManager[R]) initState() {
	store, err := GetStore[R](c.client, c.conf.DeviceId)
	if err != nil {
		log.Error("LongPollingManager. Error first store. ", err)
		time.Sleep(5 * time.Second)
		c.loopManagerInitClient = false
		return
	}
	c.skipFirstState = !c.conf.FirstStateFromServer
	if c.conf.FirstStateFromServer {
		c.Events <- *store
	}
	c.loopManagerInitClient = true
}

func (c *LongPollingManager[R]) loopState() {
	store, err := GetLongPollingStore[R](c.client, c.conf.DeviceId, c.loopManagerSinceTime)
	if err != nil {
		log.Error("LongPollingManager. Timeout long polling state", err)
		c.delayer.Delay()
		return
	}
	c.delayer.ResetDelay()
	c.loopManagerSinceTime = store.Timestamp
	if store.Timestamp > 0 && store.Timeout != "" {
		// Timeout long polling
		return
	}
	// Skip first state
	if c.skipFirstState {
		c.skipFirstState = false
		return
	}
	c.Events <- store.Data
}

func (c *LongPollingManager[R]) RunForever() {
	for {
		if !c.loopManagerInitClient {
			c.initState()
			continue
		}
		c.loopState()
	}
}

func (c *LongPollingManager[R]) Shutdown() {
	close(c.stopSignal)
}
