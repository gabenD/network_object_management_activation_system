package delayer

import (
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"
)

type Delayer struct {
	delays []int
	index  int
}

func NewDelayer() *Delayer {
	return &Delayer{
		delays: []int{
			0,
			5,
			10,
			30,
			30,
			60,
			120,
			300,
			600,
		},
		index: 0,
	}
}

func (d *Delayer) Delay() {
	if d.index >= len(d.delays) {
		d.index = len(d.delays) - 1
	}
	if d.index <= 0 {
		d.index = 0
	}
	duration := d.delays[d.index]
	d.index++
	log.Debug(fmt.Sprintf("Delaying for %v...\n", duration))
	if duration > 0 {
		time.Sleep(time.Duration(duration) * time.Second)
	}
}

func (d *Delayer) ResetDelay() {
	d.index = 0
}
