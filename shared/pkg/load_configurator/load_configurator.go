package load_configurator

import (
	"encoding/json"
	"flag"
	"os"

	log "github.com/sirupsen/logrus"
)

func LoadConfigFromArguments[R any]() (*R, error) {
	configPath := flag.String("config", "config.json", "set config json file")
	flag.Parse()
	return LoadConfig[R](*configPath)
}

func LoadConfig[R any](file string) (*R, error) {
	byteValue, err := os.ReadFile(file)
	if err != nil {
		return nil, err
	}
	var config R
	err = json.Unmarshal(byteValue, &config)
	return &config, err
}

func SetLogger(debug *bool) {
	log.SetFormatter(&log.JSONFormatter{DisableHTMLEscape: true})
	if *debug {
		log.SetLevel(log.DebugLevel)
	} else {
		log.SetLevel(log.InfoLevel)
	}
}
