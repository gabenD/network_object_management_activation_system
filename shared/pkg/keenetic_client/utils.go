package keenetic_client

import (
	"crypto/md5"
	"crypto/sha256"
	"encoding/hex"
	"io"
)

type KeenAuth struct {
	Login    string `json:"login"`
	Password string `json:"password"`
}

type KeenHotspotInterface struct {
	ID          string `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
}

type KeenHotspotResponseItem struct {
	Mac        string                `json:"mac"`
	Ip         string                `json:"ip"`
	Hostname   string                `json:"hostname"`
	Name       string                `json:"name"`
	Interface  *KeenHotspotInterface `json:"interface"`
	Registered bool                  `json:"registered"`
	Access     string                `json:"access"`
	Schedule   string                `json:"schedule"`
	Priority   int                   `json:"priority"`
	Active     bool                  `json:"active"`
	UpTime     uint32                `json:"uptime"`
	Link       string                `json:"link"`
}

type KeenHotspotShowIpHotspotHost struct {
	Host []KeenHotspotResponseItem `json:"host"`
}

func NewKeenAuth(login string, password string, xNDMRealm string, xNDMChallenge string) *KeenAuth {
	hashMd5 := md5.New()
	io.WriteString(hashMd5, login+":"+xNDMRealm+":"+password)
	hexMd5 := hex.EncodeToString(hashMd5.Sum(nil))
	hashSha256 := sha256.New()
	hashSha256.Write([]byte(xNDMChallenge + hexMd5))
	var hashPassword string = hex.EncodeToString(hashSha256.Sum(nil))
	return &KeenAuth{
		Login:    login,
		Password: hashPassword,
	}
}
