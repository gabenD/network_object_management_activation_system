package keenetic_client

import (
	"errors"
)

var KeeneticAuthException = errors.New("authorization is not completed")
