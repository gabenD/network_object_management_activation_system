package keenetic_client

import (
	"time"

	"github.com/go-resty/resty/v2"
)

type KeeneticClientConfig struct {
	Login    string
	Password string
	BaseUrl  string
}

type KeeneticClient struct {
	config *KeeneticClientConfig
	debug  *bool
	client *resty.Client
}

func NewKeeneticClient(config *KeeneticClientConfig, debug *bool) *KeeneticClient {
	client := resty.New()
	client.SetDebug(*debug)
	client.SetHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0")
	client.SetTimeout(1 * time.Minute)
	client.SetBaseURL(config.BaseUrl)
	client.SetHeader("Accept", "application/json")
	return &KeeneticClient{
		config: config,
		debug:  debug,
		client: client,
	}
}

func (c *KeeneticClient) auth() error {
	const url = "/auth"
	resp, err := c.client.R().Get(url)
	if err != nil {
		return err
	}
	if resp.StatusCode() >= 200 && resp.StatusCode() < 300 {
		return nil
	}
	xNDMRealm := resp.Header().Get("X-NDM-Realm")
	xNDMChallenge := resp.Header().Get("X-NDM-Challenge")
	payload := NewKeenAuth(c.config.Login, c.config.Password, xNDMRealm, xNDMChallenge)
	resp, err = c.client.R().SetBody(payload).Post(url)
	if err != nil {
		return err
	}
	if resp.StatusCode() >= 200 && resp.StatusCode() < 300 {
		return nil
	}
	return KeeneticAuthException
}

func (c *KeeneticClient) Wake(mac string) error {
	const url = "/rci/ip/hotspot/wake"
	err := c.auth()
	if err != nil {
		return err
	}
	_, err = c.client.R().SetBody(`{"mac":"` + mac + `"}`).Post(url)
	return err

}

func (c *KeeneticClient) List() (*KeenHotspotShowIpHotspotHost, error) {
	const url = "/rci/show/ip/hotspot"
	err := c.auth()
	result := &KeenHotspotShowIpHotspotHost{}
	if err != nil {
		return result, err
	}
	resp, err := c.client.R().SetBody(`{}`).SetResult(result).Post(url)
	if resp.StatusCode() >= 200 && resp.StatusCode() < 300 {
		return result, err
	}
	return nil, KeeneticAuthException
}
