package notion_utils

import "github.com/dstotijn/go-notion"

func GetAllText(properties *notion.DatabasePageProperties, title string) string {
	result := ""
	if properties == nil {
		return result
	}
	if property, ok := (*properties)[title]; ok {
		for _, text := range property.Title {
			result += text.Text.Content
		}
		for _, text := range property.RichText {
			result += text.Text.Content
		}
		if property.Select != nil {
			result += property.Select.Name
		}
	}
	return result
}

func GetAllNumber(properties *notion.DatabasePageProperties, title string) float64 {
	result := 0.00
	if properties == nil {
		return result
	}
	property, ok := (*properties)[title]
	if !ok {
		return result
	}
	if property.Number == nil {
		return result
	}
	return *property.Number
}

func GetAllDate(properties *notion.DatabasePageProperties, title string) *notion.DateTime {
	if properties == nil {
		return nil
	}
	if property, ok := (*properties)[title]; ok {
		if property.Date == nil {
			return nil
		}
		return &property.Date.Start
	}
	return nil
}
