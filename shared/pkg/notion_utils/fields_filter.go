package notion_utils

import (
	"context"
	"fmt"

	"github.com/dstotijn/go-notion"
	log "github.com/sirupsen/logrus"
)

func AllFieldsFilterExecute(
	NotionClient *notion.Client,
	PageID string,
	Filter *notion.DatabaseQueryFilter,
	Sorts []notion.DatabaseQuerySort,
) []notion.Page {
	startCursor := ""
	result := make([]notion.Page, 0)
	log.Debug(
		fmt.Sprintf("AllFieldsFilterExecute args: PageID=%+v Filter=%+v sorts=%+v", PageID, Filter, Sorts),
	)
	for {
		rows, err := NotionClient.QueryDatabase(
			context.Background(),
			PageID,
			&notion.DatabaseQuery{
				Filter:      Filter,
				Sorts:       Sorts,
				StartCursor: startCursor,
				PageSize:    NotionPageMaxSize,
			},
		)
		if err != nil {
			log.Error(
				"AllFieldsFilterExecute exception: ",
				err,
				" startCursor: ",
				startCursor,
			)
			return result
		}
		for _, row := range rows.Results {
			result = append(result, row)
		}
		if !rows.HasMore || rows.NextCursor == nil {
			break
		}
		startCursor = *rows.NextCursor
	}
	return result
}
