package notion_utils

type NotionApiConf struct {
	Token                             string `json:"token"`
	OperationPageID                   string `json:"operations_page_id"`
	IncomeExpensesPageID              string `json:"income_expenses_page_id"`
	DashboardStatisticPageID          string `json:"dashboard_statistic_page_id"`
	GeneratorDashboardStatisticPageID string `json:"generator_dashboard_statistic_page_id"`
}
