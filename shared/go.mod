module nomas_shared

go 1.21

require (
	github.com/dstotijn/go-notion v0.11.0
	github.com/go-resty/resty/v2 v2.13.1
	github.com/sirupsen/logrus v1.9.3
)

require (
	golang.org/x/net v0.25.0 // indirect
	golang.org/x/sys v0.20.0 // indirect
)
