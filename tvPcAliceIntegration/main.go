package main

import (
	"nomas_shared/pkg/load_configurator"
	"nomas_shared/pkg/long_polling_client"
	"nomas_tv_pc_alice_integration/pkg/configurator"
	"nomas_tv_pc_alice_integration/pkg/tv_pc_polling"
	"nomas_tv_pc_alice_integration/pkg/tv_virtual_device"

	log "github.com/sirupsen/logrus"
)

func main() {
	conf, err := load_configurator.LoadConfigFromArguments[configurator.TvPcAliceConf]()
	if err != nil {
		log.Panic(err)
	}
	load_configurator.SetLogger(&conf.Debug)

	tv := tv_virtual_device.TvVirtualDevice{Channels: conf.Channels}

	callbacks := []func(
		*tv_pc_polling.ModelTV,
		*long_polling_client.LongPollingClient,
	){func(
		device *tv_pc_polling.ModelTV,
		ctx *long_polling_client.LongPollingClient,
	) {
		log.Debug("callback", device)
		switch device.Options.LastSetMethod {
		case "power":
			// Выключение
			if device.Power.Value == 0 {
				tv.Off()
				return
			}
		case "mute":
			// Управление громкостью
			if device.Mute.Value == 1 {
				tv.Mute()
			} else {
				tv.Unmute()
			}
		case "volume":
			tv.SetVolume(device.Volume.Value * 65535 / 1000)
		case "channel":
			tv.SetChannel(device.Channel.Value / 5)
		}
	}}
	tvPolling, err := tv_pc_polling.NewModelTVObserver(
		&conf.IoT,
		&conf.Debug,
		&callbacks,
	)
	if err != nil {
		log.Panic(err)
	}
	defer tvPolling.Shutdown()
	tvPolling.RunForever()
}
