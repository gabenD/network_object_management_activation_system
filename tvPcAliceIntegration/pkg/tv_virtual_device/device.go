package tv_virtual_device

import (
	"fmt"

	log "github.com/sirupsen/logrus"
)

type TvVirtualDevice struct {
	Channels []TvChannelConf
}

func (c *TvVirtualDevice) Off() {
	log.Debug("TvVirtualDevice exec command off")
	err := backgroundCommand("cmd", "/C", "%windir%\\System32\\shutdown.exe", "-l")
	if err != nil {
		log.Error("TvVirtualDevice exec command off exception: ", err)
	}
}

func (c *TvVirtualDevice) Mute() {
	log.Debug("TvVirtualDevice exec command mute")
	err := backgroundCommand("nircmd.exe", "mutesysvolume", "1")
	if err != nil {
		log.Error("TvVirtualDevice exec command mute exception: ", err)
	}
}

func (c *TvVirtualDevice) Unmute() {
	log.Debug("TvVirtualDevice exec command unmute")
	err := backgroundCommand("nircmd.exe", "mutesysvolume", "0")
	if err != nil {
		log.Error("TvVirtualDevice exec command unmute exception: ", err)
	}
}

// SetVolume value from 0 to 65535
func (c *TvVirtualDevice) SetVolume(value int) {
	valueString := fmt.Sprintf("%d", value)
	log.Debug("TvVirtualDevice exec command setVolume: ", valueString)
	err := backgroundCommand("nircmd.exe", "setsysvolume", valueString)
	if err != nil {
		log.Error("TvVirtualDevice exec command setVolume exception: ", err)
	}
}

func (c *TvVirtualDevice) SetChannel(channel int) {
	log.Debug("TvVirtualDevice exec command setChannel: ", channel)
	for _, channelConf := range c.Channels {
		if channelConf.Channel == channel {
			backgroundCommands(channelConf.Commands)
			startChromeURL(channelConf.Url)
			return
		}
	}
}
