package tv_virtual_device

type TvChannelConf struct {
	Name     string   `json:"name"`
	Channel  int      `json:"channel"`
	Commands []string `json:"commands"`
	Url      string   `json:"url"`
}
