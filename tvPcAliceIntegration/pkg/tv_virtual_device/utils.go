package tv_virtual_device

import (
	"os/exec"
	"time"

	log "github.com/sirupsen/logrus"
)

func backgroundCommand(name string, arg ...string) error {
	cmd := exec.Command(name, arg...)
	if err := cmd.Start(); err != nil {
		return err
	}
	return nil
}

func backgroundCommands(commands []string) {
	for _, command := range commands {
		err := backgroundCommand(command)
		if err != nil {
			log.Error("TvVirtualDevice exec command SetChannel exception: ", err)
			return
		}
		if len(command) > 1 {
			time.Sleep(2 * time.Second)
		}
	}
	return
}

func startChromeURL(url string) {
	if len(url) <= 0 {
		return
	}
	_ = backgroundCommand(`c:\Program Files\Google\Chrome\Application\chrome.exe`,
		url,
	)
	return
}
