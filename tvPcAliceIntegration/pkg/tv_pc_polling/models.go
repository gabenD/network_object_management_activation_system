package tv_pc_polling

type ModelAnyOptions struct {
	LastSetMethod string `json:"last_set_method"`
}

type ModelTV struct {
	Power   ModelTVPowerValue   `json:"power"`
	Volume  ModelTVVolumeValue  `json:"volume"`
	Channel ModelTVChannelValue `json:"channel"`
	Mute    ModelTVMuteValue    `json:"mute"`
	Options ModelAnyOptions     `json:"_options"`
}

type ModelTVChannelValue struct {
	Value int `json:"value"  validate:"min=0,max=99999"`
}

type ModelTVPowerValue struct {
	Value int `json:"value"  validate:"min=0,max=1"`
}

type ModelTVVolumeValue struct {
	Value int `json:"value"  validate:"min=0,max=1000"`
}

type ModelTVMuteValue struct {
	Value int `json:"value"  validate:"min=0,max=1"`
}
