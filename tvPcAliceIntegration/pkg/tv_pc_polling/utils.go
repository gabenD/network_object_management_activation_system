package tv_pc_polling

import (
	"nomas_shared/pkg/long_polling_client"
)

func NewModelTVObserver(
	config *long_polling_client.Conf,
	debug *bool,
	callbacks *[]func(
		*ModelTV,
		*long_polling_client.LongPollingClient,
	),
) (*long_polling_client.LongPollingManager[ModelTV], error) {
	confManager, confClient := long_polling_client.ParseConf(config)
	client := long_polling_client.NewLongPollingClient(
		confClient,
		debug,
	)
	return long_polling_client.StartLongPolling[ModelTV](confManager, client, callbacks)
}
