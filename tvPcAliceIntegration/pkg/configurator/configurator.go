package configurator

import (
	"nomas_shared/pkg/long_polling_client"
	"nomas_tv_pc_alice_integration/pkg/tv_virtual_device"
)

type TvPcAliceConf struct {
	Debug    bool                              `json:"debug"`
	IoT      long_polling_client.Conf          `json:"iot"`
	Channels []tv_virtual_device.TvChannelConf `json:"channels"`
}
