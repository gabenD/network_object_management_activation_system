module nomas_tv_pc_alice_integration

go 1.21

require github.com/sirupsen/logrus v1.9.3

require (
	github.com/stretchr/testify v1.8.4 // indirect
	golang.org/x/sys v0.13.0 // indirect
)
