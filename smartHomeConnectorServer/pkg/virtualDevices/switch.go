package virtualDevices

import (
	"nomas_shared/pkg/keenetic_client"

	log "github.com/sirupsen/logrus"
)

type SwitchModelAnyOptions struct {
	LastSetMethod string `json:"last_set_method"`
}

type SwitchModelPowerValue struct {
	Value int `json:"value"  validate:"min=0,max=1"`
}

type SwitchModelProperty struct {
	Power   SwitchModelPowerValue `json:"power"`
	Options SwitchModelAnyOptions `json:"_options"`
}

type PowerWOLVirtualDeviceExecutor struct {
	wolClient  *keenetic_client.KeeneticClient
	deviceID   string
	macAddress string
}

func NewPowerWOLVirtualDeviceExecutor(deviceID string, macAddress string, client *keenetic_client.KeeneticClient) *PowerWOLVirtualDeviceExecutor {
	return &PowerWOLVirtualDeviceExecutor{
		wolClient:  client,
		deviceID:   deviceID,
		macAddress: macAddress,
	}
}

func (c *PowerWOLVirtualDeviceExecutor) DeviceID() string {
	return c.deviceID
}

func (c *PowerWOLVirtualDeviceExecutor) Execute(payload map[string]interface{}) error {
	device := SwitchModelProperty{}
	err := payloadUnmarshal(payload, &device)
	if err != nil {
		log.Error("Произошла ошибка с PowerWOLVirtualDeviceExecutor. Error: ", err)
		return err
	}
	switch device.Options.LastSetMethod {
	case "power":
		// Включение
		if device.Power.Value == 1 {
			log.Debug("PowerWOLVirtualDeviceExecutor выполняет действие - включение")
			err = c.wolClient.Wake(c.macAddress)
			if err != nil {
				log.Error("Произошла ошибка с PowerWOLVirtualDeviceExecutor.KeeneticClient. Error: ", err)
				return err
			}
		}
	}
	return nil
}
