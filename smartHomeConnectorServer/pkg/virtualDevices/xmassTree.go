package virtualDevices

import (
	"nomas_shared/pkg/gyverTwinkClient"
	"nomas_shared/pkg/virtualDevices"

	log "github.com/sirupsen/logrus"
)

type XmasTreeVirtualDeviceExecutor struct {
	deviceID  string
	ipAddress string
	client    *gyverTwinkClient.GyverTwinkClient
}

func NewXmasTreeVirtualDeviceExecutor(
	deviceID string,
	ipAddress string,
) *XmasTreeVirtualDeviceExecutor {
	return &XmasTreeVirtualDeviceExecutor{
		deviceID:  deviceID,
		ipAddress: ipAddress,
		client:    gyverTwinkClient.NewGyverTwinkClient(),
	}
}

func (c *XmasTreeVirtualDeviceExecutor) DeviceID() string {
	return c.deviceID
}

func EffectFromColor(color string) uint8 {
	// Оранжевый
	if color == "ff3c00" {
		return gyverTwinkClient.GyverTwinkEffectSunsetGrad
	}
	// Жёлтый
	if color == "#ff700a" {
		return gyverTwinkClient.GyverTwinkEffectSunsetNoise
	}
	// Розовый
	if color == "#ff0a99" {
		return gyverTwinkClient.GyverTwinkEffectPinkGrad
	}
	// Лиловый
	if color == "#ff2b36" {
		return gyverTwinkClient.GyverTwinkEffectPinkNoise
	}
	// Красный
	if color == "#ff0a0a" {
		return gyverTwinkClient.GyverTwinkEffectRedWhiteGrad
	}
	// Малиновый
	if color == "#ff0055" {
		return gyverTwinkClient.GyverTwinkEffectRedWhiteNoise
	}
	// Cиний
	if color == "#0a47ff" {
		return gyverTwinkClient.GyverTwinkEffectPepsiGrad
	}
	// Голубой
	if color == "#0ad6ff" {
		return gyverTwinkClient.GyverTwinkEffectPepsiNoise
	}
	// Фиолетовый
	if color == "#ad0aff" {
		return gyverTwinkClient.GyverTwinkEffectCyberGrad
	}
	// Сиреневый
	if color == "#850aff" {
		return gyverTwinkClient.GyverTwinkEffectCyberNoise
	}
	// Бирюзовый
	if color == "#0afff3" {
		return gyverTwinkClient.GyverTwinkEffectStripeGrad
	}
	// Изумродный
	if color == "#0affad" {
		return gyverTwinkClient.GyverTwinkEffectStripeNoise
	}
	// Лунный
	if color == "#e5e9ff" {
		return gyverTwinkClient.GyverTwinkEffectRaibowGrad
	}
	// Салатовый
	if color == "#caff0a" {
		return gyverTwinkClient.GyverTwinkEffectPartyGrad
	}
	// Зелёный??
	return gyverTwinkClient.GyverTwinkEffectNext
}

func EffectFromTemperature(temp int64) uint8 {
	// Мягкий белый
	if temp >= 0 && temp <= 2700 {
		return gyverTwinkClient.GyverTwinkEffectWarmGrad
	}
	// Тёплый белый
	if temp <= 3400 {
		return gyverTwinkClient.GyverTwinkEffectHotGrad
	}
	if temp <= 4500 {
		return gyverTwinkClient.GyverTwinkEffectHotNoise
	}
	// Дненвной белый
	if temp <= 5600 {
		return gyverTwinkClient.GyverTwinkEffectColdGrad
	}
	// Холодный белый
	return gyverTwinkClient.GyverTwinkEffectColdNoise
}

func (c *XmasTreeVirtualDeviceExecutor) Execute(payload map[string]interface{}) error {
	device := virtualDevices.ModelRGBLamp{}
	err := payloadUnmarshal(payload, &device)
	if err != nil {
		log.Error("Произошла ошибка с PowerWOLVirtualDeviceExecutor. Error: ", err)
		return err
	}
	switch device.Options.LastSetMethod {
	case "power":
		// Выключение
		if device.Power.Value == 0 {
			return c.client.PowerOff(c.ipAddress)
		}
		// Включение
		if device.Power.Value == 1 {
			return c.client.PowerOn(c.ipAddress)
		}
	case "color":
		var effect uint8
		if device.Color.Type == "temperature" {
			effect = EffectFromTemperature(device.Color.Temperature)
		} else {
			effect = EffectFromColor(device.Color.Hex)
		}
		return c.client.SetEffect(c.ipAddress, effect)
	case "brightness":
		brightness := uint8(virtualDevices.BrightnessMap(
			int(device.Brightness.Value), 0, 100, 0, 255),
		)
		return c.client.SetBrightness(c.ipAddress, brightness)
	}
	return nil
}
