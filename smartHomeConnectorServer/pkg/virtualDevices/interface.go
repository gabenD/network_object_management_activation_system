package virtualDevices

type VirtualDeviceExecutor interface {
	DeviceID() string
	Execute(payload map[string]interface{}) error
}
