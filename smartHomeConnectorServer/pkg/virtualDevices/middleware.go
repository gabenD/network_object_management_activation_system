package virtualDevices

import log "github.com/sirupsen/logrus"

type VirtualDeviceMiddleware struct {
	Devices []VirtualDeviceExecutor
}

func NewVirtualDeviceMiddleware() *VirtualDeviceMiddleware {
	devices := make([]VirtualDeviceExecutor, 0)
	return &VirtualDeviceMiddleware{
		Devices: devices,
	}
}

func (c *VirtualDeviceMiddleware) Add(dev VirtualDeviceExecutor) *VirtualDeviceMiddleware {
	c.Devices = append(c.Devices, dev)
	return c
}

func (c *VirtualDeviceMiddleware) Execute(deviceID string, payload map[string]interface{}) {
	log.Debug("Получили event для обработки виртуальным устройством deviceID: ", deviceID, " payload: ", payload)
	for _, device := range c.Devices {
		if device.DeviceID() != deviceID {
			continue
		}
		log.Debug("Обработка сообщения виртуальным устройством deviceID: ", deviceID)
		_ = device.Execute(payload)
	}
}
