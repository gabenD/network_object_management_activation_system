package virtualDevices

import (
	"encoding/json"
)

func payloadUnmarshal(payload map[string]interface{}, v any) error {
	jsonBody, err := json.Marshal(payload)
	if err != nil {
		return err
	}
	if err := json.Unmarshal(jsonBody, v); err != nil {
		return err
	}
	return nil
}
