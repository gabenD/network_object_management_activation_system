package configurator

import (
	"nomas_smart_home_connector_server/pkg/iotStore"
	"nomas_smart_home_connector_server/server"
)

type KeeneticApiConf struct {
	Login    string `json:"login"`
	Password string `json:"password"`
	BaseUrl  string `json:"base_url"`
}

type SwitchPCWolDevice struct {
	ID         string `json:"id"`
	MacAddress string `json:"mac_address"`
}

type XmasTreeGarlandDevice struct {
	ID string `json:"id"`
	Ip string `json:"ip"`
}
type VirtualDevicesConf struct {
	SwitchPC    SwitchPCWolDevice     `json:"switch_pc"`
	XmasGarland XmasTreeGarlandDevice `json:"xmas_tree_garland_device"`
}

type SmartHomeConnectorServerConf struct {
	Debug          bool               `json:"debug"`
	Server         server.Conf        `json:"server"`
	IotStore       iotStore.Conf      `json:"iot"`
	Keenetic       KeeneticApiConf    `json:"keenetic"`
	VirtualDevices VirtualDevicesConf `json:"virtual_devices"`
}
