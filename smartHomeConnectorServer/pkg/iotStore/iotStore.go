package iotStore

import (
	"encoding/json"
	"errors"
	"fmt"
	"nomas_smart_home_connector_server/pkg/virtualDevices"
	"os"

	log "github.com/sirupsen/logrus"
)

type RequestPayload struct {
	Token  string      `json:"token"`
	Device string      `json:"device"`
	Action string      `json:"action"`
	Method string      `json:"method"`
	Params interface{} `json:"params"`
}

type ResponseLongPolling struct {
	Event    string
	Response *map[string]interface{}
}
type Response struct {
	Response            *map[string]interface{}
	ResponseError       bool
	ErrorCode           int
	ErrorMessage        string
	ResponseLongPolling *ResponseLongPolling
}

type StoreService struct {
	config         *Conf
	folderTemplate string
	Middleware     *virtualDevices.VirtualDeviceMiddleware `json:"middleware"`
}

const OptionsStoreKey = "_options"

func NewIotStoreService(config *Conf) *StoreService {
	service := StoreService{
		config:         config,
		folderTemplate: "%s/alice-data-%s.json",
	}
	service.GenerateWorkingDirectory()
	return &service
}

func (c *StoreService) SetMiddleware(middleware *virtualDevices.VirtualDeviceMiddleware) *StoreService {
	c.Middleware = middleware
	return c
}

func (c *StoreService) GenerateWorkingDirectory() {
	log.Info("StoreService.GenerateWorkingDirectory")
	_ = os.MkdirAll(c.config.FolderName, 0777)
	for _, device := range c.config.Devices {
		filename := c.GetFileNameByDevice(device.Id)
		if _, err := os.Stat(filename); errors.Is(err, os.ErrNotExist) {
			_ = c.storeSave(device.Id, map[string]interface{}{})
		}
	}
}

func (c *StoreService) GetFileNameByDevice(device string) string {
	return fmt.Sprintf(c.folderTemplate, c.config.FolderName, device)
}

func (c *StoreService) storeSave(device string, data map[string]interface{}) error {
	filename := c.GetFileNameByDevice(device)
	jsonContent, err := json.Marshal(data)
	if err != nil {
		log.Error("StoreService", err)
		return err
	}
	err = os.WriteFile(filename, jsonContent, 0755)
	if err != nil {
		log.Error("StoreService", err)
		return err
	}
	return nil
}

func (c *StoreService) storeGet(device string) (map[string]interface{}, error) {
	filename := c.GetFileNameByDevice(device)
	raw, err := os.ReadFile(filename)
	if err != nil {
		log.Error("StoreService", err)
		return nil, err
	}
	var data map[string]interface{}
	err = json.Unmarshal(raw, &data)
	if err != nil {
		log.Error("StoreService", err)
		return nil, err
	}
	return data, nil
}

func (c *StoreService) getOrDefault(val interface{}, def interface{}) interface{} {
	if val != nil {
		return val
	}
	return def
}

func (c *StoreService) ExistsDevices(device string) bool {
	for _, v := range c.config.Devices {
		if v.Id == device {
			return true
		}
	}
	return false
}

func (c *StoreService) checkTokenDevice(token string, device string) bool {
	for _, v := range c.config.Devices {
		if v.Id == device && v.Token != "" && v.Token == token {
			return true
		}
	}
	return false
}

func (c *StoreService) checkRequestToken(request *RequestPayload) bool {
	switch request.Action {
	case "get", "set":
		return c.config.Token == request.Token
	case "store":
		return true
	case "store-device":
		return c.checkTokenDevice(request.Token, request.Device)
	default:
		return false
	}
}

func (c *StoreService) Execute(request *RequestPayload) *Response {
	if !c.checkRequestToken(request) {
		return &Response{ResponseError: true, ErrorCode: 403, ErrorMessage: "Unauthorized"}
	}
	if !c.ExistsDevices(request.Device) {
		return &Response{ResponseError: true, ErrorCode: 400, ErrorMessage: "Invalid device"}
	}
	switch request.Action {
	case "get":
		log.Info("StoreService get action")
		return c.executeGetAction(request)
	case "set":
		log.Info("StoreService set action")
		return c.executeSetAction(request)
	case "store":
		log.Info("StoreService store action")
		return c.executeStoreAction(request)
	case "store-device":
		log.Info("StoreService store-device action")
		return c.executeStoreDeviceAction(request)
	default:
		return &Response{ResponseError: true, ErrorCode: 500, ErrorMessage: "Invalid action"}
	}
}

func (c *StoreService) executeGetAction(request *RequestPayload) *Response {
	store, err := c.storeGet(request.Device)
	if err != nil {
		return &Response{ResponseError: true, ErrorCode: 500, ErrorMessage: "Error getting store"}
	}
	storeMethod, ok := store[request.Method].(map[string]interface{})
	if !ok {
		storeMethod = make(map[string]interface{})
	}
	output := make(map[string]interface{})
	for _, value := range request.Params.([]interface{}) {
		valueString := value.(string)
		output[valueString] = c.getOrDefault(storeMethod[valueString], nil)
	}
	return &Response{Response: &output, ResponseError: false}
}

func (c *StoreService) executeSetAction(request *RequestPayload) *Response {
	store, err := c.storeGet(request.Device)
	if err != nil {
		return &Response{ResponseError: true, ErrorCode: 500, ErrorMessage: "Error getting store"}
	}
	store[request.Method] = request.Params
	store[OptionsStoreKey] = map[string]interface{}{
		"last_set_method": request.Method,
	}
	err = c.storeSave(request.Device, store)
	if err != nil {
		return &Response{ResponseError: true, ErrorCode: 500, ErrorMessage: "Error saving store"}
	}
	output := map[string]interface{}{"status": "ok"}
	log.Info("StoreService preparing new state")
	longPollingResponse := ResponseLongPolling{
		Event:    fmt.Sprintf("%s_%s", "EventLongPollingDevices", request.Device),
		Response: c.executeStoreAction(request).Response,
	}
	log.Info("StoreService longPolling prepared new state")
	if c.Middleware != nil {
		go c.Middleware.Execute(request.Device, store)
	}
	return &Response{Response: &output, ResponseError: false, ResponseLongPolling: &longPollingResponse}
}

func (c *StoreService) executeStoreAction(request *RequestPayload) *Response {
	store, err := c.storeGet(request.Device)
	if err != nil {
		return &Response{ResponseError: true, ErrorCode: 500, ErrorMessage: "Error getting store"}
	}
	return &Response{Response: &store, ResponseError: false}
}

func (c *StoreService) executeStoreDeviceAction(request *RequestPayload) *Response {
	err := c.storeSave(request.Device, request.Params.(map[string]interface{}))
	if err != nil {
		return &Response{ResponseError: true, ErrorCode: 500, ErrorMessage: "Error saving device-store"}
	}
	output := map[string]interface{}{"status": "ok"}
	log.Info("StoreService device send your new state")
	return &Response{Response: &output, ResponseError: false}
}
