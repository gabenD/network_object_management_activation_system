package iotStore

type Conf struct {
	Token      string       `json:"token"`
	Devices    []DeviceConf `json:"devices"`
	FolderName string       `json:"folder"`
}

type DeviceConf struct {
	Id    string `json:"id"`
	Token string `json:"token"`
}
