package main

import (
	"nomas_shared/pkg/keenetic_client"
	"nomas_shared/pkg/load_configurator"
	"nomas_smart_home_connector_server/pkg/configurator"
	"nomas_smart_home_connector_server/pkg/iotStore"
	"nomas_smart_home_connector_server/pkg/virtualDevices"
	"nomas_smart_home_connector_server/server"

	log "github.com/sirupsen/logrus"
)

func initVirtualDevices(conf *configurator.SmartHomeConnectorServerConf) *virtualDevices.VirtualDeviceMiddleware {
	keeneticClient := keenetic_client.NewKeeneticClient(
		&keenetic_client.KeeneticClientConfig{
			Login:    conf.Keenetic.Login,
			Password: conf.Keenetic.Password,
			BaseUrl:  conf.Keenetic.BaseUrl,
		},
		&conf.Debug,
	)
	switchPC := virtualDevices.NewPowerWOLVirtualDeviceExecutor(
		conf.VirtualDevices.SwitchPC.ID,
		conf.VirtualDevices.SwitchPC.MacAddress,
		keeneticClient,
	)
	xmasGarland := virtualDevices.NewXmasTreeVirtualDeviceExecutor(
		conf.VirtualDevices.XmasGarland.ID,
		conf.VirtualDevices.XmasGarland.Ip,
	)
	return virtualDevices.NewVirtualDeviceMiddleware().Add(switchPC).Add(xmasGarland)
}

func main() {
	conf, err := load_configurator.LoadConfigFromArguments[configurator.SmartHomeConnectorServerConf]()
	if err != nil {
		log.Panic(err)
	}
	load_configurator.SetLogger(&conf.Debug)
	devices := initVirtualDevices(conf)
	iotStoreService := iotStore.NewIotStoreService(&conf.IotStore).SetMiddleware(devices)
	fiberApp := server.NewFiberServer(
		&server.Dependency{ServerConfig: &conf.Server, IotStoreService: iotStoreService, Debug: &conf.Debug},
	)
	log.Info("Starting server")
	fiberApp()
}
