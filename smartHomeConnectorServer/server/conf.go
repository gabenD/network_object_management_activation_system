package server

type Conf struct {
	Port string `json:"port"`
}

const longPollingMaxTimeoutSeconds = 5 * 60
