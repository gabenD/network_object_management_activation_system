package server

import (
	"encoding/json"
	"errors"
	"fmt"
	"nomas_smart_home_connector_server/pkg/iotStore"
	"time"

	log "github.com/sirupsen/logrus"

	lp "github.com/LdDl/fiber-long-poll/v2"
	"github.com/gofiber/fiber/v2"
	"github.com/valyala/fasthttp"
)

func NewFiberServer(dependency *Dependency) func() {
	config := fiber.Config{
		ErrorHandler: func(ctx *fiber.Ctx, err error) error {
			log.Error(err)
			return ctx.Status(fasthttp.StatusInternalServerError).JSON(errors.New("panic error"))
		},
		IdleTimeout: 10 * time.Second,
	}
	app := fiber.New(config)
	manager, err := lp.StartLongpoll(lp.Options{
		LoggingEnabled:                 *dependency.Debug,
		MaxLongpollTimeoutSeconds:      longPollingMaxTimeoutSeconds,
		MaxEventBufferSize:             100,
		EventTimeToLiveSeconds:         longPollingMaxTimeoutSeconds,
		DeleteEventAfterFirstRetrieval: false,
	})
	if err != nil {
		panic(err)
	}
	setRouter(app, manager, dependency.IotStoreService)
	return func() {
		defer manager.Shutdown()
		port := fmt.Sprintf(":%s", dependency.ServerConfig.Port)
		err := app.Listen(port)
		if err != nil {
			log.Error("Can't start server due the error: %s\n", err.Error())
		}
	}
}

func setRouter(app *fiber.App, manager *lp.LongpollManager, iotStoreService *iotStore.StoreService) {
	app.Post("/devices", func(c *fiber.Ctx) error {
		var inputDTO iotStore.RequestPayload
		err := json.Unmarshal(c.Body(), &inputDTO)
		if err != nil {
			log.Error("Devices", err)
			return fiber.NewError(fiber.StatusBadRequest, "Invalid JSON format")
		}
		outputDTO := iotStoreService.Execute(&inputDTO)
		if outputDTO.ResponseError {
			return fiber.NewError(outputDTO.ErrorCode, outputDTO.ErrorMessage)
		}
		if outputDTO.ResponseLongPolling != nil && outputDTO.ResponseLongPolling.Response != nil {
			log.Info("Preparing ResponseLongPolling")
			err = manager.Publish(
				outputDTO.ResponseLongPolling.Event,
				interface{}(*outputDTO.ResponseLongPolling.Response),
			)
		}
		if err != nil {
			log.Error("LongPollingPublishError", err)
		}
		return c.JSON(outputDTO.Response)
	})

	app.Get("/longPolling/:device", func(c *fiber.Ctx) error {
		device := c.Params("device")
		sinceTime := c.Query("since_time", "0")
		if !iotStoreService.ExistsDevices(device) {
			log.Info("longPolling", device)
			return fiber.NewError(fiber.StatusBadRequest, "Invalid device")
		}
		c.Context().PostArgs().Set("timeout", fmt.Sprintf("%d", longPollingMaxTimeoutSeconds))
		c.Context().PostArgs().Set("since_time", sinceTime)
		c.Context().PostArgs().Set("category", fmt.Sprintf("%s_%s", "EventLongPollingDevices", device))
		return manager.SubscriptionHandler(c)
	})
}
