package server

import "nomas_smart_home_connector_server/pkg/iotStore"

type Dependency struct {
	ServerConfig    *Conf                  `json:"conf"`
	IotStoreService *iotStore.StoreService `json:"iot"`
	Debug           *bool                  `json:"debug"`
}
