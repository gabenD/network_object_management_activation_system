import copy
import json
from abc import ABC, abstractmethod

from automation.client import AlexStarSimpleClient


class AutomationBaseRule:
    def __init__(
            self,
            rule_name: str,
            answer: str,
            rule_payload_without_device_and_token: dict
    ):
        self.rule_name = rule_name
        self.answer = answer
        self.rule_payload_without_device_and_token = rule_payload_without_device_and_token

    def payload(self, token: str, device: str) -> str:
        pl = copy.deepcopy(self.rule_payload_without_device_and_token)
        pl["token"] = token
        pl["device"] = device
        resp = json.dumps(pl, ensure_ascii=False, indent=4)
        return resp.replace("\"<<", "").replace(">>\"", "")


class AutomationBase(ABC):
    def __init__(
            self,
            name: str,
            device: str,
            token: str,
            url: str,
            param_ssl_check: bool = False,
            client: AlexStarSimpleClient = None,
    ):
        self.name = name
        self.device = device
        self.token = token
        self.url = url
        self.param_ssl_check = param_ssl_check
        self._client = client

    def set_client(self, client: AlexStarSimpleClient) -> 'AutomationBase':
        self._client = client
        return self

    @abstractmethod
    def rules(self) -> list[AutomationBaseRule]:
        ...

    def create(self) -> 'AutomationBase':
        for rule in self.rules():
            payload = rule.payload(token=self.token, device=self.device)
            rule_new = self._client.post(data={"action": "new", "rule_type": "1"})
            rule_id = rule_new["rule"]["id"]
            self._client.post(
                data={
                    "id": rule_id,
                    "name": rule.answer,
                    "command": f"{self.name} {rule.rule_name}",
                    "action_url": self.url,
                    "value_type": 0,
                    "payload": payload,
                    "active": 1,
                    "valid": 1,
                    "can_replay": 1,
                    "server_id": None,
                    "url_encode": 0,
                    "rule_type": 1,
                    "rule_sub_type": 4,
                    "qos": 0,
                    "retain": 1,
                    "topic": None,
                    "clientid": None,
                    "ssl_check": int(self.param_ssl_check),
                    "is_exit": 0,
                    "action": "save",
                }
            )
        return self
