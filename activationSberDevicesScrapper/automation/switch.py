from automation.base import AutomationBaseRule, AutomationBase


class Switch(AutomationBase):
    def rules(self) -> list[AutomationBaseRule]:
        return [
            AutomationBaseRule(
                rule_name="[0] Правило на включение",
                answer="Включаю",
                rule_payload_without_device_and_token={
                    "action": "set",
                    "method": "power",
                    "params": {
                        "value": 1
                    }
                }
            ),
            AutomationBaseRule(
                rule_name="[1] Правило на выключение",
                answer="Выключаю",
                rule_payload_without_device_and_token={
                    "action": "set",
                    "method": "power",
                    "params": {
                        "value": 0
                    }
                }
            ),
            AutomationBaseRule(
                rule_name="[2] Запрос состояния вкл выкл",
                answer="Запрашиваю",
                rule_payload_without_device_and_token={
                    "action": "get",
                    "method": "power",
                    "params": [
                        "value"
                    ]
                }
            ),
        ]
