import requests
import requests.cookies
import logging

logger = logging.getLogger(__name__)


class AlexStarSimpleClient:
    def __init__(
            self,
            cookies_session_id: str,
            user_agent: str,
    ):
        self._base_url = "https://alexstar.ru/smarthome?"
        self._cookies_session_id = cookies_session_id
        self._user_agent = user_agent
        self._session = requests.Session()
        cookie_obj = requests.cookies.create_cookie(
            domain="alexstar.ru",
            name="PHPSESSID",
            value=cookies_session_id
        )
        self._session.cookies.set_cookie(cookie_obj)
        self._session.headers.update({
            'User-Agent': user_agent,
            'X-Requested-With': 'XMLHttpRequest',
            "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
        })

    def post(self, data: dict) -> dict:
        response = self._session.post(
            url=self._base_url,
            data=data
        )
        logger.debug(f"Запрос на AlexStar {response.status_code} {response.text}")
        return response.json()
