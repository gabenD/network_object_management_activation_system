from automation.client import AlexStarSimpleClient
from automation.lamp import Lamp


def main():
    Lamp(
        name="Лампочка_1",
        device="guid-device-Лампочка_1",
        token="tome-token",
        url="https://localhost:5000/devices",
    ).set_client(
        client=AlexStarSimpleClient(
            cookies_session_id="",
            user_agent="Mozilla/5.0 Chrome/119.0.0.0 Safari/537.36",
        )
    ).create()


if __name__ == "__main__":
    import logging

    logging.basicConfig(level=logging.DEBUG)
    main()
