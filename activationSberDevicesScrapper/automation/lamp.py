from automation.base import AutomationBaseRule, AutomationBase


class Lamp(AutomationBase):
    def rules(self) -> list[AutomationBaseRule]:
        return [
            AutomationBaseRule(
                rule_name="[0] Правило на включение",
                answer="Включаю",
                rule_payload_without_device_and_token={
                    "action": "set",
                    "method": "power",
                    "params": {
                        "value": 1
                    }
                }
            ),
            AutomationBaseRule(
                rule_name="[1] Правило на выключение",
                answer="Выключаю",
                rule_payload_without_device_and_token={
                    "action": "set",
                    "method": "power",
                    "params": {
                        "value": 0
                    }
                }
            ),
            AutomationBaseRule(
                rule_name="[2] Запрос состояния вкл выкл",
                answer="Запрашиваю",
                rule_payload_without_device_and_token={
                    "action": "get",
                    "method": "power",
                    "params": [
                        "value"
                    ]
                }
            ),
            AutomationBaseRule(
                rule_name="[3] Правило для запрос яркости",
                answer="Запрашиваю яркость",
                rule_payload_without_device_and_token={
                    "action": "get",
                    "method": "brightness",
                    "params": [
                        "value"
                    ]
                }
            ),
            AutomationBaseRule(
                rule_name="[4] Правило для управления яркостью",
                answer="Меняю яркость",
                rule_payload_without_device_and_token={
                    "action": "set",
                    "method": "brightness",
                    "params": {
                        "value": "<<{value}>>"
                    }
                }
            ),
            AutomationBaseRule(
                rule_name="[5] Задание цвета в формате rgb",
                answer="Меняю цвет",
                rule_payload_without_device_and_token={
                    "action": "set",
                    "method": "color",
                    "params": {
                        "color": "<<{value}>>",
                        "r": "<<{r}>>",
                        "g": "<<{g}>>",
                        "b": "<<{b}>>",
                        "#rgb": "{#rgb}",
                        "type": "rgb",
                    }
                }
            ),
            AutomationBaseRule(
                rule_name="[6] Запрос цвета",
                answer="Запрашиваю цвет",
                rule_payload_without_device_and_token={
                    "action": "get",
                    "method": "color",
                    "params": [
                        "color"
                    ]
                }
            ),
            AutomationBaseRule(
                rule_name="[7] Запрос цветовой температуры",
                answer="Запрашиваю температуру",
                rule_payload_without_device_and_token={
                    "action": "get",
                    "method": "color",
                    "params": [
                        "temperature"
                    ]
                }
            ),
            AutomationBaseRule(
                rule_name="[8] Установка цветовой температуры",
                answer="Меняю температуру",
                rule_payload_without_device_and_token={
                    "action": "set",
                    "method": "color",
                    "params": {
                        "temperature": "<<{value}>>",
                        "type": "temperature",
                    }
                }
            ),
        ]
