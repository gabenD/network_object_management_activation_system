from automation.base import AutomationBaseRule, AutomationBase


class TV(AutomationBase):
    def rules(self) -> list[AutomationBaseRule]:
        return [
            AutomationBaseRule(
                rule_name="[00] Правило на включение",
                answer="Включаю",
                rule_payload_without_device_and_token={
                    "action": "set",
                    "method": "power",
                    "params": {
                        "value": 1
                    }
                }
            ),
            AutomationBaseRule(
                rule_name="[1] Правило на выключение",
                answer="Выключаю",
                rule_payload_without_device_and_token={
                    "action": "set",
                    "method": "power",
                    "params": {
                        "value": 0
                    }
                }
            ),
            AutomationBaseRule(
                rule_name="[2] Запрос состояния вкл выкл",
                answer="Запрашиваю",
                rule_payload_without_device_and_token={
                    "action": "get",
                    "method": "power",
                    "params": [
                        "value"
                    ]
                }
            ),
            AutomationBaseRule(
                rule_name="[3] Запрос громкости",
                answer="Запрашиваю громкость",
                rule_payload_without_device_and_token={
                    "action": "get",
                    "method": "volume",
                    "params": [
                        "value"
                    ]
                }
            ),
            AutomationBaseRule(
                rule_name="[4] Правило для управления громкостью",
                answer="Управляю громкостью",
                rule_payload_without_device_and_token={
                    "action": "set",
                    "method": "volume",
                    "params": {
                        "value": "<<{value}>>"
                    }
                }
            ),
            AutomationBaseRule(
                rule_name="[5] Запрос канала",
                answer="Запрашиваю канал",
                rule_payload_without_device_and_token={
                    "action": "get",
                    "method": "channel",
                    "params": [
                        "value"
                    ]
                }
            ),
            AutomationBaseRule(
                rule_name="[6] Правило для управления каналом",
                answer="Управляю каналами",
                rule_payload_without_device_and_token={
                    "action": "set",
                    "method": "channel",
                    "params": {
                        "value": "<<{value}>>"
                    }
                }
            ),
            AutomationBaseRule(
                rule_name="[7] Правило для управления mute",
                answer="Управляю громкость",
                rule_payload_without_device_and_token={
                    "action": "set",
                    "method": "mute",
                    "params": {
                        "value": "<<{value}>>"
                    }
                }
            ),
            AutomationBaseRule(
                rule_name="[8] Запрос mute",
                answer="Отдаю mute",
                rule_payload_without_device_and_token={
                    "action": "get",
                    "method": "mute",
                    "params": [
                        "value"
                    ]
                }
            ),
            AutomationBaseRule(
                rule_name="[9] Запрос подсветки",
                answer="Запрашиваю состояние подсветки",
                rule_payload_without_device_and_token={
                    "action": "get",
                    "method": "backlight",
                    "params": [
                        "value"
                    ]
                }
            ),
            AutomationBaseRule(
                rule_name="[10] Правило для управления подсветкой",
                answer="Управляю подсветкой",
                rule_payload_without_device_and_token={
                    "action": "set",
                    "method": "backlight",
                    "params": {
                        "value": "<<{value}>>"
                    }
                }
            ),
            AutomationBaseRule(
                rule_name="[11] Запрос блокировки управления",
                answer="Запрашиваю состояние блокировки управления",
                rule_payload_without_device_and_token={
                    "action": "get",
                    "method": "control_lock",
                    "params": [
                        "value"
                    ]
                }
            ),
            AutomationBaseRule(
                rule_name="[12] Правило для управления блокировкой управления",
                answer="Управляю блокировкой управления",
                rule_payload_without_device_and_token={
                    "action": "set",
                    "method": "control_lock",
                    "params": {
                        "value": "<<{value}>>"
                    }
                }
            ),
            AutomationBaseRule(
                rule_name="[13] Управление источником",
                answer="Отдаю источник",
                rule_payload_without_device_and_token={
                    "action": "get",
                    "method": "source",
                    "params": [
                        "value"
                    ]
                }
            ),
            AutomationBaseRule(
                rule_name="[14] Правило для управления источником",
                answer="Управляю источником",
                rule_payload_without_device_and_token={
                    "action": "set",
                    "method": "source",
                    "params": {
                        "value": "{value}"
                    }
                }
            ),
        ]
