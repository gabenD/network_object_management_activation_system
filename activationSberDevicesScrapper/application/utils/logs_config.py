import logging.config
from .configuration import config

LOGLEVEL = "DEBUG" if config.is_debug else "INFO"
SETTINGS_LOGGER = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {"default": {"()": "application.utils.logs.JsonFormatter"}},
    "handlers": {"default": {"class": "logging.StreamHandler", "formatter": "default"}},
    "loggers": {
        "": {"level": LOGLEVEL, "handlers": ["default"]},
    },
}
logging.config.dictConfig(SETTINGS_LOGGER)
logging.getLogger().setLevel(LOGLEVEL)
