import argparse

from pydantic import BaseModel, Field


class Config(BaseModel):
    cookies_store_path: str = Field(..., title="Путь для хранения cookies")
    barer_security_token: str = Field(..., title="Токен для получения доступа")
    port: int = Field(..., title="Порт HTTP сервера", ge=5000)
    is_debug: bool = False

    @classmethod
    def from_file(cls, path: str) -> 'Config':
        with open(path) as fin:
            return Config.model_validate_json(fin.read(), strict=False)


def parse_config_object():
    parser = argparse.ArgumentParser()
    parser.add_argument('--config', default='../config.json')
    args = parser.parse_args()
    return Config.from_file(path=args.config)


config = parse_config_object()
