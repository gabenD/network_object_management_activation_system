from application.utils.logs_config import SETTINGS_LOGGER  # noqa
from application.utils import config

if __name__ == "__main__":
    from application.run import app

    app.run(host="0.0.0.0", port=config.port, debug=config.is_debug, threaded=False)
