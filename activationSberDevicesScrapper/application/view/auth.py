from logging import getLogger

from flask import abort, request
from application.app import app
from application.services.cookies_saver import SaveCookies, use_chrome_driver, get_token_info, TokenCookieResponse
from application.services.page_scrapper import PageScrapper
from application.utils import config

logger = getLogger(__name__)


@app.route("/api/v1/activation_sber/token", methods=['POST'])
def activation_sber():
    expires = request.args.get('expires', default=0, type=int)
    authorization = request.headers.get('Authorization') or ""
    if authorization != f"Bearer {config.barer_security_token}":
        abort(403, description="Invalid authentication credentials")
    logger.info(f"Получение токенов по {expires=}")
    cookies_saver = SaveCookies(cookie_file=config.cookies_store_path)
    token_cookies = get_token_info(cookies_saver=cookies_saver)
    if expires is not None and token_cookies.cookies and expires < token_cookies.expires:
        return token_cookies.model_dump()
    logger.info(f"Expires info {expires=} {token_cookies.expires=}")
    with use_chrome_driver(cookies_saver=cookies_saver) as chrome_driver:
        PageScrapper(driver=chrome_driver).execute()
    token_cookies = get_token_info(cookies_saver=cookies_saver)
    return token_cookies.model_dump()
