import logging
import random
import time

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By

logger = logging.getLogger(__name__)


class PageScrapper:
    def __init__(self, driver: webdriver.Chrome):
        self.driver = driver

    @staticmethod
    def wait(count: int = 1):
        time.sleep(random.randint(2, 3 * count))

    def log_current_state(self):
        logger.info(f"Текущий URL: {self.driver.current_url}")
        logger.debug(self.driver.find_element(By.XPATH, '//body').get_attribute('innerHTML'))

    def click_accessibly_button(self) -> bool:
        try:
            element = self.driver.find_element(By.XPATH, "//button//*[contains(text(), 'Понятно')]")
            element.click()
            logger.info("Нажали на элемент 'Понятно'")
            return True
        except NoSuchElementException:
            logger.info("Нет элемента 'Понятно'")
            return False

    def click_login_header_page(self) -> bool:
        elements = self.driver.find_elements(
            By.XPATH,
            "//button//*[contains(text(), 'Войти')]"
        )
        if not elements:
            logger.info("Нет элемента 'Войти'")
            return False
        logger.info(f"Найдено {len(elements)} c кнопкой 'Войти' нажимаем на первую")
        elements[0].click()
        logger.info("Нажали на элемент 'Войти'")
        return True

    def click_login_popup_page(self) -> bool:
        elements = self.driver.find_elements(
            By.XPATH,
            "//button//*[contains(text(), 'Войти')]"
        )
        if not elements:
            logger.info("Нет элемента 'Войти' в popup")
            return False
        logger.info(f"Найдено {len(elements)} c кнопкой 'Войти' нажимаем на последнюю")
        elements[-1].click()
        logger.info("Нажали на элемент 'Войти' в popup")
        return True

    def click_auto_continue_sber(self) -> bool:
        try:
            element = self.driver.find_element(
                By.XPATH,
                "//button//*[contains(text(), 'Продолжить')]"
            )
            element.click()
            logger.info("Нажали на элемент 'Продолжить'")
            return True
        except NoSuchElementException:
            logger.info("Нет элемента 'Продолжить'")
            return False

    def attempt_complete_state(self):
        logger.info("Попытка получить токен для управления умным домом")
        self.wait(count=3)
        self.driver.get('https://activation.sber.ru/account/services')
        self.wait(count=3)
        self.log_current_state()
        return

    def execute(self):
        self.driver.get('https://activation.sber.ru')
        self.driver.set_page_load_timeout(5 * 60)  # 5 minutes
        self.wait()
        self.click_accessibly_button()
        if not self.click_login_header_page():
            logger.info("Актуальный токен доступа")
            return
        self.wait()
        if not self.click_login_popup_page():
            logger.info("Может быть актуальный токен доступа")
            logger.debug(self.driver.find_element(By.XPATH, '//body').get_attribute('innerHTML'))
            return
        self.wait()
        if not self.driver.current_url.startswith('https://online.sberbank.ru'):
            logger.info("Не сработал редирект на SberID")
            self.attempt_complete_state()
            return
        self.wait(count=3)
        if not self.click_auto_continue_sber():
            logger.info("Автоматический вход не сработал по SberID")
            self.attempt_complete_state()
            return
        logger.info("Автоматический вход сработал. Получение ihacs токена")
        self.attempt_complete_state()
