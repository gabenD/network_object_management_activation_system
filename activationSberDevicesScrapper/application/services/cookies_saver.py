import json
import logging
import random
from contextlib import contextmanager
import time

from pydantic import BaseModel
from pyvirtualdisplay import Display
from selenium.webdriver.chrome.options import Options
from selenium import webdriver

logger = logging.getLogger(__name__)


class SaveCookies:
    def __init__(self, cookie_file: str):
        self.cookie_file = cookie_file
        self.responses_cookies = {
            "ihapi_access",
        }
        self.selenium_not_set_cookies = {
            "ihapi_access",
        }

    def save_cookies(self, cookies: list[dict]):
        logger.info(f"Saving cookies in {self.cookie_file}")
        with open(self.cookie_file, "w") as fp:
            json.dump(cookies, fp, ensure_ascii=False, default=str, indent=4)

    @property
    def load_cookies(self) -> list[dict]:
        logger.info(f"Loading cookies from {self.cookie_file}")
        with open(self.cookie_file, "r") as fp:
            cookies = json.load(fp)
        # Iterate through pickle dict and add all the cookies
        for cookie in cookies:
            # Fix issue Chrome exports 'expiry' key but expects 'expire' on import
            if 'expiry' in cookie:
                cookie['expires'] = cookie['expiry']
                del cookie['expiry']
            if 'expires' in cookie:
                cookie['expires'] = int(cookie['expires'])
        return cookies

    def set_cookies_on_driver(self, driver: webdriver.Chrome, cookies: list[dict]):
        # Enables network tracking so we may use Network.setCookie method
        driver.execute_cdp_cmd('Network.enable', {})
        for cookie in cookies:
            if cookie.get('name') in self.selenium_not_set_cookies:
                continue
            driver.execute_cdp_cmd('Network.setCookie', cookie)
        # Disable network tracking
        driver.execute_cdp_cmd('Network.disable', {})

    @staticmethod
    def get_cookies_from_driver(driver: webdriver.Chrome) -> list[dict]:
        sites = (
            "https://activation.sber.ru",
            "https://online.sberbank.ru",
        )
        cookies = []
        for site in sites:
            driver.get(site)
            time.sleep(random.randint(4, 10))
            cookies.extend(driver.get_cookies())
        return cookies


@contextmanager
def use_chrome_driver(cookies_saver: SaveCookies) -> webdriver.Chrome:
    # set xvfb display since there is no GUI in docker container.
    display = Display(visible=0, size=(414, 869))  # Iphone XR resolution
    display.start()

    chrome_options = Options()
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--disable-dev-shm-usage')

    logger.info('building session')
    chrome_driver = webdriver.Chrome(options=chrome_options)
    cookies_saver.set_cookies_on_driver(driver=chrome_driver, cookies=cookies_saver.load_cookies)
    yield chrome_driver
    cookies_saver.save_cookies(cookies=cookies_saver.get_cookies_from_driver(driver=chrome_driver))
    # close chromedriver and display
    chrome_driver.quit()
    display.stop()


class TokenCookieWithName(BaseModel):
    name: str
    domain: str
    path: str = "/"
    value: str
    expires: int


class TokenCookieResponse(BaseModel):
    cookies: list[TokenCookieWithName] = []
    expires: int = 0


def get_token_info(cookies_saver: SaveCookies) -> TokenCookieResponse:
    for cookie in cookies_saver.load_cookies:
        if cookie.get('name') in cookies_saver.responses_cookies:
            token = TokenCookieWithName.model_validate(cookie, strict=False)
            return TokenCookieResponse(
                cookies=[token],
                expires=token.expires,
            )
    return TokenCookieResponse()
