from flask import Flask


def get_application() -> Flask:
    _app = Flask(__name__)
    return _app


app = get_application()
