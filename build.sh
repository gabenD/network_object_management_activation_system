#!/bin/zsh
BUILD_DIRECTORY=dist/addons
rm -rf dist/* || 1
# razerAliceIntegration
echo "Build razerAliceIntegration"
PROJECT=razerAliceIntegration
mkdir -p $BUILD_DIRECTORY/$PROJECT
cd $PROJECT && rm -rf dist/* || 1
GOOS=windows GOARCH=386 go build -a -installsuffix cgo -o dist/main.exe
GOOS=windows GOARCH=amd64 go build -a -installsuffix cgo -o dist/main64.exe
cp main.bat dist/main.bat
cp launch_bat.vbs dist/launch_bat.vbs
cp config-simple.json dist/config-simple.json
cp -r dist/* ../$BUILD_DIRECTORY/$PROJECT
cd ..

# smartHomeConnectorServer
echo "Build smartHomeConnectorServer"
PROJECT=smartHomeConnectorServer
mkdir -p $BUILD_DIRECTORY/$PROJECT
cd $PROJECT && rm -rf dist/* || 1
GOOS=linux GOARCH=arm GOARM=7 go build -a -installsuffix cgo -o dist/smartHomeConnectorServer
chmod +x dist/smartHomeConnectorServer
cp -r smart_home_connector_server/* dist
cp config-simple.json dist/config-simple.json
cp -r dist/* ../$BUILD_DIRECTORY/$PROJECT
cd ..

# telegramAutomation
echo "Build telegramAutomation"
PROJECT=telegramAutomation
mkdir -p $BUILD_DIRECTORY/$PROJECT
cd $PROJECT && rm -rf dist/* || 1
GOOS=linux GOARCH=arm GOARM=7 go build -a -installsuffix cgo -o dist/telegramAutomation
chmod +x dist/telegramAutomation
cp -r telegram_automation/* dist
cp config-simple.json dist/config-simple.json
cp -r dist/* ../$BUILD_DIRECTORY/$PROJECT
cd ..

# razerAliceIntegration
echo "Build tvPcAliceIntegration"
PROJECT=tvPcAliceIntegration
mkdir -p $BUILD_DIRECTORY/$PROJECT
cd $PROJECT && rm -rf dist/* || 1
GOOS=windows GOARCH=386 go build -a -installsuffix cgo -o dist/main.exe
GOOS=windows GOARCH=amd64 go build -a -installsuffix cgo -o dist/main64.exe
cp main.bat dist/main.bat
cp launch_bat.vbs dist/launch_bat.vbs
cp config-simple.json dist/config-simple.json
cp -r dist/* ../$BUILD_DIRECTORY/$PROJECT
cd ..

# metabaseDashboard
echo "Build metabaseDashboard"
PROJECT=metabaseDashboard
mkdir -p $BUILD_DIRECTORY/$PROJECT
cd $PROJECT && rm -rf dist/* || 1
CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -o dist/metabaseDashboard
cp docker-compose.yaml dist/docker-compose.yaml
cp Dockerfile dist/Dockerfile
cp config-simple.json dist/config-simple.json
cp -r dist/* ../$BUILD_DIRECTORY/$PROJECT
cd ..
