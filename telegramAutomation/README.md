Для установки компонента в HomeAssistant используй гайд:

https://developers.home-assistant.io/docs/add-ons/tutorial

1. Переходи в папку /addons
2. Копируй папку с докер-образом в telegram_automation
3. Создай в ней config.json и настрой все параметры
4. Перенеси бинарник telegramAutomation, так чтобы его полный путь был
   /addons/telegram_automation/telegramAutomation
5. Запускай расширение вот тут https://my.home-assistant.io/redirect/supervisor_store/ (Local addons)

Если меняешь config.json придётся удалить весь AddOn или придумай способ сам
