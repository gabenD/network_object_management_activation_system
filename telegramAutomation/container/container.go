package container

import (
	"nomas_shared/pkg/keenetic_client"
	"nomas_shared/pkg/metabase_sync_client"
	"nomas_telegram_wol/pkg/bot"
	"nomas_telegram_wol/pkg/configurator"
	"nomas_telegram_wol/pkg/useCase"

	"github.com/dstotijn/go-notion"
)

type DI struct {
	config                 *configurator.TelegramWOLConf
	bot                    *bot.TelegramBot
	NotionAddTinkoffFileUC *useCase.NotionAddTinkoffFileUC
}

func NewDI(config *configurator.TelegramWOLConf) *DI {
	di := DI{}
	di.config = config
	botTg := bot.NewTelegramBotInit(
		&config.Telegram,
		&config.Debug,
	)
	// Bot
	di.bot = &bot.TelegramBot{
		Config: &config.Telegram,
		Bot:    botTg,
		Debug:  &config.Debug,
	}
	// UseCases
	keeneticClient := keenetic_client.NewKeeneticClient(
		&keenetic_client.KeeneticClientConfig{
			Login:    config.Keenetic.Login,
			Password: config.Keenetic.Password,
			BaseUrl:  config.Keenetic.BaseUrl,
		},
		&config.Debug,
	)
	NotionSyncUC := useCase.NotionAddTinkoffFileUC{
		NotionClient: notion.NewClient(config.Notion.Token),
		Conf:         &config.Notion,
	}
	di.NotionAddTinkoffFileUC = &NotionSyncUC
	tinkoffSyncFileUC := useCase.TinkoffSyncFileUseCase{
		BotAPI:       botTg,
		NotionSyncUC: &NotionSyncUC,
	}
	notionIncomeExpensesSyncUC := useCase.NotionIncomeExpensesSyncUC{
		NotionClient: notion.NewClient(config.Notion.Token),
		Conf:         &config.Notion,
	}
	metabaseSyncUC := useCase.MetabaseSyncUseCase{
		MetabaseClient: metabase_sync_client.NewMetabaseSyncClient(&config.Metabase, &config.Debug),
	}
	deviceKeeneticUC := useCase.DeviceKeeneticUseCase{
		KeeneticClient: keeneticClient,
	}
	service := bot.HandlerDirector{
		TgContainer: di.bot,
		Bot:         botTg,
		Handlers: []bot.TelegramHandler{
			&bot.HandlerCheckAllowUser{},
			&bot.HandlerDevices{
				UseCase: &deviceKeeneticUC,
			},
			&bot.HandlerBankOfficer{},
			&bot.HandlerMetabaseSync{
				UseCase: &metabaseSyncUC,
			},
			&bot.HandlerBankOfficerTinkoff{
				UseCase: &tinkoffSyncFileUC,
			},
			&bot.HandlerBankOfficerSync{},
			&bot.HandlerBankOfficerSyncAll{
				UseCase: &notionIncomeExpensesSyncUC,
			},
			&bot.HandlerDefault{},
		},
	}
	di.bot.Service = &service
	return &di
}

func (c *DI) Start() {
	c.bot.Start()
}
