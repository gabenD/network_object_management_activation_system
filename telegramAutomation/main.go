package main

import (
	"nomas_shared/pkg/load_configurator"
	"nomas_telegram_wol/container"
	"nomas_telegram_wol/pkg/configurator"

	log "github.com/sirupsen/logrus"
)

func main() {
	conf, err := load_configurator.LoadConfigFromArguments[configurator.TelegramWOLConf]()
	if err != nil {
		log.Panic(err)
	}
	load_configurator.SetLogger(&conf.Debug)
	di := container.NewDI(conf)
	di.Start()
}
