module nomas_telegram_wol

go 1.21

require (
	github.com/dstotijn/go-notion v0.11.0
	github.com/go-telegram-bot-api/telegram-bot-api/v5 v5.5.1
	github.com/gocarina/gocsv v0.0.0-20240520201108-78e41c74b4b1
	github.com/sirupsen/logrus v1.9.3
	github.com/thoas/go-funk v0.9.3
	golang.org/x/text v0.14.0
)

require (
	github.com/stretchr/testify v1.8.1 // indirect
	golang.org/x/sys v0.15.0 // indirect
	golang.org/x/xerrors v0.0.0-20190717185122-a985d3407aa7 // indirect
)
