package configurator

type KeeneticApiConf struct {
	Login    string `json:"login"`
	Password string `json:"password"`
	BaseUrl  string `json:"base_url"`
}
