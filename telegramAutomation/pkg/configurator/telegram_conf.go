package configurator

type TelegramConf struct {
	Token   string   `json:"token"`
	Timeout int      `json:"timeout"`
	UserIds []string `json:"user_ids"`
}
