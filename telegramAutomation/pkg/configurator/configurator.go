package configurator

import (
	"nomas_shared/pkg/metabase_sync_client"
	"nomas_shared/pkg/notion_utils"
)

type TelegramWOLConf struct {
	Debug    bool                                        `json:"debug"`
	Telegram TelegramConf                                `json:"telegram"`
	Keenetic KeeneticApiConf                             `json:"keenetic"`
	Notion   notion_utils.NotionApiConf                  `json:"notion"`
	Metabase metabase_sync_client.MetabaseSyncClientConf `json:"metabase"`
}
