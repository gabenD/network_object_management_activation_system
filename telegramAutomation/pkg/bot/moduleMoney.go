package bot

import (
	"nomas_telegram_wol/pkg/useCase"

	log "github.com/sirupsen/logrus"
)

type HandlerBankOfficer struct {
}

func (c *HandlerBankOfficer) Handle(dto *PipelineBotInput) *PipelineBotOutput {
	if dto.state.Module == "" && dto.message == string(BankOfficer) {
		dto.state.Module = "BankOfficer"
		return &PipelineBotOutput{
			keyboard: &BankOfficerKeyboard,
			state:    dto.state,
			message:  "Хорошо. Выбери операции по счету",
		}
	}
	if dto.state.Module != "BankOfficer" {
		return nil
	}
	if dto.message != string(Back) {
		return nil
	}
	dto.state.Module = ""
	return &PipelineBotOutput{
		keyboard: &MainKeyboard,
		state:    dto.state,
		message:  "Возвращаюсь в главное меню",
	}
}

type HandlerBankOfficerTinkoff struct {
	UseCase *useCase.TinkoffSyncFileUseCase
}

func checkMimetypeCSV(MimeType string) bool {
	switch MimeType {
	case "text/csv", "application/csv", "text/plain":
		return true
	default:
		return false
	}
}
func (c *HandlerBankOfficerTinkoff) Handle(dto *PipelineBotInput) *PipelineBotOutput {
	if dto.state.Module == "BankOfficer" && dto.message == string(BankOfficerTinkoff) {
		dto.state.Module = "BankOfficerTinkoff"
		return &PipelineBotOutput{
			keyboard: &BackKeyboard,
			state:    dto.state,
			message:  "Пришли мне CSV файл - список операций по счетам",
		}
	}
	if dto.state.Module != "BankOfficerTinkoff" {
		return nil
	}
	if dto.message == string(Back) {
		dto.state.Module = ""
		return &PipelineBotOutput{
			keyboard: &MainKeyboard,
			state:    dto.state,
			message:  "Возвращаюсь в главное меню",
		}
	}
	document := dto.update.Message.Document
	if document == nil {
		return &PipelineBotOutput{
			keyboard: &BackKeyboard,
			state:    dto.state,
			message:  "Пожалуйста, пришли сообщение c CSV файлом",
		}
	}

	if !checkMimetypeCSV(document.MimeType) {
		log.Info("Пользователь пытается загрузить файл формата ", document.MimeType)
		return &PipelineBotOutput{
			keyboard: &BackKeyboard,
			state:    dto.state,
			message:  "Файл должен быть строго CSV формата",
		}
	}
	message := c.UseCase.Execute(document.FileID)
	dto.state.Module = ""
	return &PipelineBotOutput{
		keyboard: &MainKeyboard,
		state:    dto.state,
		message:  message,
	}
}
