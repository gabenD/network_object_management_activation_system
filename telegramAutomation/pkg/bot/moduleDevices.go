package bot

import (
	"nomas_telegram_wol/pkg/useCase"
	"strings"
)

type HandlerDevices struct {
	UseCase *useCase.DeviceKeeneticUseCase
}

func (c *HandlerDevices) Handle(dto *PipelineBotInput) *PipelineBotOutput {
	const prefix = "/deviceWakeOn_"
	command := ""
	if strings.HasPrefix(dto.message, prefix) {
		command = strings.TrimPrefix(dto.message, prefix)
	}
	if dto.state.Module == "" && (dto.message == string(Devices) || len(command) > 0) {
		return &PipelineBotOutput{
			keyboard: &MainKeyboard,
			state:    nil,
			message:  c.UseCase.Execute(command),
		}
	}
	return nil
}
