package bot

import (
	"nomas_telegram_wol/pkg/botState"
)

type HandlerDefault struct {
}

func (c *HandlerDefault) Handle(dto *PipelineBotInput) *PipelineBotOutput {
	return &PipelineBotOutput{
		keyboard: &MainKeyboard,
		state:    &botState.StructUserState{},
		message:  "Выбери пункт меню",
	}
}
