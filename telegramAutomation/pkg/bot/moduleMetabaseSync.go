package bot

import "nomas_telegram_wol/pkg/useCase"

type HandlerMetabaseSync struct {
	UseCase *useCase.MetabaseSyncUseCase
}

func (c *HandlerMetabaseSync) Handle(dto *PipelineBotInput) *PipelineBotOutput {
	if dto.state.Module != "BankOfficerSync" || dto.message != string(BankOfficerSyncMoneyChart) {
		return nil
	}
	dto.state.Module = ""
	message := c.UseCase.Execute()
	return &PipelineBotOutput{
		keyboard: &MainKeyboard,
		state:    dto.state,
		message:  message,
	}
}
