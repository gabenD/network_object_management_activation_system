package bot

import (
	"nomas_telegram_wol/pkg/botState"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

type PipelineBotInput struct {
	c       *TelegramBot
	update  *tgbotapi.Update
	message string
	userId  string
	state   *botState.StructUserState
}

type PipelineBotOutput struct {
	keyboard *tgbotapi.ReplyKeyboardMarkup
	state    *botState.StructUserState
	message  string
}
