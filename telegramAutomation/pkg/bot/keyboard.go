package bot

import tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"

type MainKeyboardBtn string

const (
	Devices                          MainKeyboardBtn = "🧊 Устройства"
	BankOfficer                      MainKeyboardBtn = "💰 Счета&Траты"
	BankOfficerTinkoff               MainKeyboardBtn = "🟨 Тинькофф"
	BankOfficerSync                  MainKeyboardBtn = "▶️ Синхронизация"
	BankOfficerSyncMoney             MainKeyboardBtn = "💲 Notion.Операции по счетам"
	BankOfficerSyncGenerateStatistic MainKeyboardBtn = "📋 Notion.Генератор статистики"
	BankOfficerSyncStatistic         MainKeyboardBtn = "📊 Notion.Статистика"
	BankOfficerSyncMoneyChart        MainKeyboardBtn = "☁️ Metabase.Графики"
	BankOfficerSyncAll               MainKeyboardBtn = "🌍 Все"
	Back                             MainKeyboardBtn = "◀️ Назад"
)

var MainKeyboard = tgbotapi.NewReplyKeyboard(
	tgbotapi.NewKeyboardButtonRow(
		tgbotapi.NewKeyboardButton(string(Devices)),
	),
	tgbotapi.NewKeyboardButtonRow(
		tgbotapi.NewKeyboardButton(string(BankOfficer)),
	),
)

var BackKeyboard = tgbotapi.NewReplyKeyboard(
	tgbotapi.NewKeyboardButtonRow(
		tgbotapi.NewKeyboardButton(string(Back)),
	),
)

var BankOfficerKeyboard = tgbotapi.NewReplyKeyboard(
	tgbotapi.NewKeyboardButtonRow(
		tgbotapi.NewKeyboardButton(string(BankOfficerTinkoff)),
	),
	tgbotapi.NewKeyboardButtonRow(
		tgbotapi.NewKeyboardButton(string(BankOfficerSync)),
	),
	tgbotapi.NewKeyboardButtonRow(
		tgbotapi.NewKeyboardButton(string(Back)),
	),
)

var BankOfficerSyncKeyboard = tgbotapi.NewReplyKeyboard(
	tgbotapi.NewKeyboardButtonRow(
		tgbotapi.NewKeyboardButton(string(BankOfficerSyncMoney)),
	),
	tgbotapi.NewKeyboardButtonRow(
		tgbotapi.NewKeyboardButton(string(BankOfficerSyncGenerateStatistic)),
	),
	tgbotapi.NewKeyboardButtonRow(
		tgbotapi.NewKeyboardButton(string(BankOfficerSyncStatistic)),
	),
	tgbotapi.NewKeyboardButtonRow(
		tgbotapi.NewKeyboardButton(string(BankOfficerSyncMoneyChart)),
	),
	tgbotapi.NewKeyboardButtonRow(
		tgbotapi.NewKeyboardButton(string(BankOfficerSyncAll)),
	),
	tgbotapi.NewKeyboardButtonRow(
		tgbotapi.NewKeyboardButton(string(Back)),
	),
)
