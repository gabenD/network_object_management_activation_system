package bot

import tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"

type TelegramHandler interface {
	Handle(dto *PipelineBotInput) *PipelineBotOutput
}

type TelegramHandlerDirector interface {
	Execute(update *tgbotapi.Update) error
}
