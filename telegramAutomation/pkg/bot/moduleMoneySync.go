package bot

import (
	"nomas_telegram_wol/pkg/useCase"
)

type HandlerBankOfficerSync struct {
}

func (c *HandlerBankOfficerSync) Handle(dto *PipelineBotInput) *PipelineBotOutput {
	if dto.state.Module == "BankOfficer" && dto.message == string(BankOfficerSync) {
		dto.state.Module = "BankOfficerSync"
		return &PipelineBotOutput{
			keyboard: &BankOfficerSyncKeyboard,
			state:    dto.state,
			message:  "Выбери пункт для синхронизации",
		}
	}
	if dto.state.Module == "BankOfficerSync" && dto.message == string(Back) {
		dto.state.Module = ""
		return &PipelineBotOutput{
			keyboard: &MainKeyboard,
			state:    dto.state,
			message:  "Возвращаюсь в главное меню",
		}
	}
	return nil
}

// Синхронизация всего

type HandlerBankOfficerSyncAll struct {
	UseCase *useCase.NotionIncomeExpensesSyncUC
}

func (c *HandlerBankOfficerSyncAll) Handle(dto *PipelineBotInput) *PipelineBotOutput {
	if dto.state.Module != "BankOfficerSync" {
		return nil
	}
	params := map[string]useCase.IncomeExpensesSync{
		string(BankOfficerSyncAll):               useCase.IncomeExpensesSyncAll,
		string(BankOfficerSyncMoney):             useCase.IncomeExpensesSyncMoney,
		string(BankOfficerSyncGenerateStatistic): useCase.IncomeExpensesSyncGenerateStatistic,
		string(BankOfficerSyncStatistic):         useCase.IncomeExpensesSyncStatistic,
		string(BankOfficerSyncMoneyChart):        useCase.IncomeExpensesSyncMoneyChart,
	}
	operation, ok := params[dto.message]
	if !ok {
		return nil
	}
	dto.state.Module = ""
	message := c.UseCase.Execute(operation)
	return &PipelineBotOutput{
		keyboard: &MainKeyboard,
		state:    dto.state,
		message:  message,
	}
}
