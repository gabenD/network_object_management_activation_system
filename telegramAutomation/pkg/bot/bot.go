package bot

import (
	"nomas_telegram_wol/pkg/configurator"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	log "github.com/sirupsen/logrus"
)

type TelegramBot struct {
	Config  *configurator.TelegramConf
	Bot     *tgbotapi.BotAPI
	Debug   *bool
	Service TelegramHandlerDirector
}

func NewTelegramBotInit(
	config *configurator.TelegramConf,
	debug *bool,
) *tgbotapi.BotAPI {
	bot, err := tgbotapi.NewBotAPI(config.Token)
	if err != nil {
		log.Panic(err)
	}
	bot.Debug = *debug
	return bot
}

func (c *TelegramBot) checkAllowUser(userId string) bool {
	for _, allowUserId := range c.Config.UserIds {
		if userId == allowUserId {
			return true
		}
	}
	return false
}

func (c *TelegramBot) getUpdatesChannel() (*tgbotapi.UpdatesChannel, error) {

	log.Printf("Authorized on account %s", c.Bot.Self.UserName)
	u := tgbotapi.NewUpdate(0)
	u.Timeout = c.Config.Timeout
	updates := c.Bot.GetUpdatesChan(u)
	return &updates, nil
}

func (c *TelegramBot) Start() {
	updates, err := c.getUpdatesChannel()
	if err != nil {
		log.Panic(err)
	}
	for update := range *updates {
		err = c.Service.Execute(&update)
		if err != nil {
			log.Panic(err)
		}
	}
}
