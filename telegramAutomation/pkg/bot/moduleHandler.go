package bot

import (
	"nomas_telegram_wol/pkg/botState"
	"strconv"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	log "github.com/sirupsen/logrus"
)

type HandlerDirector struct {
	TgContainer *TelegramBot
	Bot         *tgbotapi.BotAPI
	Handlers    []TelegramHandler
}

func (c *HandlerDirector) Execute(update *tgbotapi.Update) error {
	if update.Message == nil {
		return nil
	}
	dbState := botState.LoadBotState()
	userId := strconv.FormatInt(update.Message.Chat.ID, 10)
	dto := PipelineBotInput{
		c:       c.TgContainer,
		update:  update,
		message: update.Message.Text,
		userId:  userId,
		state:   dbState.Get(userId),
	}
	log.Debug("Обработка event'а ", dto)
	for _, handler := range c.Handlers {
		output := handler.Handle(&dto)
		if output != nil {
			log.Debug("Получили обратную связь от handler'а", output)
			msg := tgbotapi.NewMessage(dto.update.Message.Chat.ID, output.message)
			if output.state != nil {
				dbState.Upsert(userId, output.state)
				log.Debug(
					"Сохранение нового состояния по пользователю userId: ",
					userId,
					" state: ",
					output.state,
					" dbState: ",
					dbState,
				)
				_ = botState.SaveBotState(dbState)
			}
			if output.keyboard != nil {
				msg.ReplyMarkup = output.keyboard
			}
			if _, err := c.Bot.Send(msg); err != nil {
				return err
			}
			break
		}
	}
	return nil
}
