package bot

import "fmt"

type HandlerCheckAllowUser struct {
}

func (c *HandlerCheckAllowUser) Handle(dto *PipelineBotInput) *PipelineBotOutput {
	for _, allowUserId := range dto.c.Config.UserIds {
		if dto.userId == allowUserId {
			return nil
		}
	}
	return &PipelineBotOutput{
		keyboard: nil,
		state:    nil,
		message:  fmt.Sprintf("Нет доступа ID: `%s`", dto.userId),
	}
}
