package structParser

import (
	"crypto/sha256"
	"encoding/csv"
	"encoding/hex"
	"fmt"
	"io"
	"strconv"
	"strings"
	"time"

	"github.com/gocarina/gocsv"
	"golang.org/x/text/encoding/charmap"
)

type DateTimeCSV struct {
	time.Time
}

// MarshalCSV Convert the internal date as CSV string
func (date *DateTimeCSV) MarshalCSV() (string, error) {
	return date.String(), nil
}

// String You could also use the standard Stringer interface
func (date *DateTimeCSV) String() string {
	return date.Time.Format("02.01.2006 15:04:05")
}

// UnmarshalCSV Convert the CSV string as internal date
func (date *DateTimeCSV) UnmarshalCSV(csv string) (err error) {
	date.Time, err = time.Parse("02.01.2006 15:04:05", csv)
	return err
}

type Float64CSV struct {
	float64
}

// MarshalCSV Convert the internal float32 as CSV string
func (c *Float64CSV) MarshalCSV() (string, error) {
	return c.String(), nil
}

// String You could also use the standard Stringer interface
func (c *Float64CSV) String() string {
	// Используем fmt.Sprintf для получения строки с точкой
	str := fmt.Sprintf("%.2f", c.float64)
	// Заменяем точку на запятую
	strWithComma := strings.Replace(str, ".", ",", -1)
	return strWithComma
}

func (c *Float64CSV) Float64() float64 {
	return c.float64
}

// UnmarshalCSV Convert the CSV string as internal date
func (c *Float64CSV) UnmarshalCSV(csv string) (err error) {
	val, err := strconv.ParseFloat(strings.Replace(csv, ",", ".", -1), 64)
	c.float64 = val
	return err
}

type TinkoffTransactionItem struct {
	OperationDate     DateTimeCSV `csv:"Дата операции"`
	PaymentDate       string      `csv:"Дата платежа"`
	CardNumber        string      `csv:"Номер карты"`
	Status            string      `csv:"Статус"`
	OperationAmount   Float64CSV  `csv:"Сумма операции"`
	OperationCurrency string      `csv:"Валюта операции"`
	PaymentAmount     Float64CSV  `csv:"Сумма платежа"`
	PaymentCurrency   string      `csv:"Валюта платежа"`
	Cashback          string      `csv:"Кэшбэк"`
	Category          string      `csv:"Категория"`
	MCC               string      `csv:"MCC"`
	Description       string      `csv:"Описание"`
	Bonuses           Float64CSV  `csv:"Бонусы (включая кэшбэк)"`
	RoundingAmount    Float64CSV  `csv:"Округление на инвесткопилку"`
	TotalAmount       Float64CSV  `csv:"Сумма операции с округлением"`
}

func (t *TinkoffTransactionItem) ID() string {
	// Объединяем все значения полей в одну строку
	// Вычисляем SHA-256 хеш строки
	var sb strings.Builder

	sb.WriteString("TinkoffTransactionItem")
	sb.WriteString(t.OperationDate.String())
	sb.WriteString(t.CardNumber)
	sb.WriteString(t.Status)
	sb.WriteString(t.OperationAmount.String())
	sb.WriteString(t.OperationCurrency)
	sb.WriteString(t.PaymentAmount.String())
	sb.WriteString(t.PaymentCurrency)
	sb.WriteString(t.MCC)
	sb.WriteString(t.TotalAmount.String())

	hash := sha256.Sum256([]byte(sb.String()))
	return hex.EncodeToString(hash[:])
}

func NewTinkoffTransactionItem(in io.Reader) ([]*TinkoffTransactionItem, error) {
	gocsv.SetCSVReader(func(in io.Reader) gocsv.CSVReader {
		r := csv.NewReader(charmap.Windows1251.NewDecoder().Reader(in))
		r.Comma = ';'
		return r
	})
	clients := []*TinkoffTransactionItem{}

	if err := gocsv.Unmarshal(in, &clients); err != nil {
		return nil, err
	}
	return clients, nil
}
