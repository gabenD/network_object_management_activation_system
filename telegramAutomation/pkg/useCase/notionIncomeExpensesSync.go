package useCase

import (
	"context"
	"fmt"
	"math"
	"nomas_shared/pkg/notion_utils"
	"time"

	"github.com/dstotijn/go-notion"
	log "github.com/sirupsen/logrus"
	"github.com/thoas/go-funk"
)

const (
	NotionPageMaxSize = 9999
)

type IncomeExpensesSync int

const (
	IncomeExpensesSyncAll IncomeExpensesSync = iota
	IncomeExpensesSyncMoney
	IncomeExpensesSyncGenerateStatistic
	IncomeExpensesSyncStatistic
	IncomeExpensesSyncMoneyChart
)

type NotionIncomeExpensesSyncUC struct {
	NotionClient *notion.Client
	Conf         *notion_utils.NotionApiConf
}

func utilsGetTypeIncomeExpenses(amount float64) string {
	if amount < 0 {
		return "Расход"
	}
	return "Доход"
}

func (c *NotionIncomeExpensesSyncUC) syncMoney() string {
	skipOperationLink := false
	countErrors := 0
	rows, err := c.NotionClient.QueryDatabase(
		context.Background(),
		c.Conf.OperationPageID,
		&notion.DatabaseQuery{
			Filter: &notion.DatabaseQueryFilter{
				And: []notion.DatabaseQueryFilter{
					{
						Property: "Операция в БД [доходы/расходы]",
						DatabaseQueryPropertyFilter: notion.DatabaseQueryPropertyFilter{
							Relation: &notion.RelationDatabaseQueryFilter{
								IsEmpty: true,
							},
						},
					},
					{
						Property: "Пропустить операцию",
						DatabaseQueryPropertyFilter: notion.DatabaseQueryPropertyFilter{
							Checkbox: &notion.CheckboxDatabaseQueryFilter{
								Equals: &skipOperationLink,
							},
						},
					},
				},
			},
			PageSize: NotionPageMaxSize,
		},
	)
	if err != nil {
		return fmt.Sprintf(
			"Произошла ошибка при получении данных не синхронизированных операций в БД [доходы/расходы] %+v",
			err,
		)
	}
	if len(rows.Results) <= 0 {
		return "Все данные синхронизированы в БД [доходы/расходы]"
	}
	for _, row := range rows.Results {
		properties := row.Properties.(notion.DatabasePageProperties)
		operationID := notion_utils.GetAllText(&properties, "ID")
		description := fmt.Sprintf(
			"%s;%s;%s",
			notion_utils.GetAllText(&properties, "Описание"),
			notion_utils.GetAllText(&properties, "Категория"),
			notion_utils.GetAllText(&properties, "Банк"),
		)
		amount := notion_utils.GetAllNumber(&properties, "Сумма операции")
		amountLinkAbs := math.Abs(amount)
		paymentDate := notion_utils.GetAllDate(&properties, "Дата операции")
		log.Info("Синхронизация ", operationID, " операции в общий бюджет")
		operationCreatedInOut, err := c.NotionClient.CreatePage(context.Background(), notion.CreatePageParams{
			ParentType: notion.ParentTypeDatabase,
			ParentID:   c.Conf.IncomeExpensesPageID,
			DatabasePageProperties: &notion.DatabasePageProperties{
				"Тип": notion.DatabasePageProperty{
					Type: notion.DBPropTypeSelect,
					Select: &notion.SelectOptions{
						Name: utilsGetTypeIncomeExpenses(amount),
					},
				},
				"Дата": notion.DatabasePageProperty{
					Type: notion.DBPropTypeDate,
					Date: &notion.Date{
						Start: *paymentDate,
					},
				},
				"Название": notion.DatabasePageProperty{
					Type: notion.DBPropTypeTitle,
					Title: []notion.RichText{
						{
							Type: notion.RichTextTypeText,
							Text: &notion.Text{
								Content: description,
							},
						},
					},
				},
				"Сумма": notion.DatabasePageProperty{
					Type:   notion.DBPropTypeNumber,
					Number: &amountLinkAbs,
				},
				"Account": notion.DatabasePageProperty{
					Type: notion.DBPropTypeSelect,
					Select: &notion.SelectOptions{
						Name: "RUB",
					},
				},
			},
		})
		if err != nil {
			log.Info("Не синхронизировалась операция ID: ", operationID, "\nОшибка: ", err.Error())
			countErrors += 1
			continue
		}
		_, err = c.NotionClient.UpdatePage(context.Background(), row.ID, notion.UpdatePageParams{
			DatabasePageProperties: notion.DatabasePageProperties{
				"Операция в БД [доходы/расходы]": notion.DatabasePageProperty{
					Type: notion.DBPropTypeRelation,
					Relation: []notion.Relation{
						{
							ID: operationCreatedInOut.ID,
						},
					},
				},
			},
		})
		if err != nil {
			log.Info("Не создалась обратная связь с операцией ID: ", operationID, "\nОшибка: ", err.Error())
			countErrors += 1
			continue
		}
	}
	return fmt.Sprintf(
		"Найдено %d операций для синхронизации в БД. Произошло ошибок: %d ",
		len(rows.Results),
		countErrors,
	)
}

// syncStatistic - функция синхронизации дешборда Статистика [доходы/расходы]
func (c *NotionIncomeExpensesSyncUC) syncStatistic() string {
	skipNotSyncOperation := true
	rows, err := c.NotionClient.QueryDatabase(
		context.Background(),
		c.Conf.DashboardStatisticPageID,
		&notion.DatabaseQuery{
			Filter: &notion.DatabaseQueryFilter{
				And: []notion.DatabaseQueryFilter{
					{
						Property: "Дата",
						DatabaseQueryPropertyFilter: notion.DatabaseQueryPropertyFilter{
							Date: &notion.DatePropertyFilter{
								IsNotEmpty: true,
							},
						},
					},
					{
						Property: "Не синхронизировать",
						DatabaseQueryPropertyFilter: notion.DatabaseQueryPropertyFilter{
							Checkbox: &notion.CheckboxDatabaseQueryFilter{
								DoesNotEqual: &skipNotSyncOperation,
							},
						},
					},
				},
			},
			PageSize: NotionPageMaxSize,
		},
	)
	if err != nil {
		message := fmt.Sprintf(
			"Произошла ошибка при получении данных из Статистика [доходы/расходы] %+v",
			err,
		)
		log.Error(message)
		return message
	}
	for _, row := range rows.Results {
		properties := row.Properties.(notion.DatabasePageProperties)
		if properties == nil {
			continue
		}
		property, ok := properties["Дата"]
		if !ok {
			continue
		}
		categoryFilter := notion_utils.GetAllText(&properties, "Категория")
		accountFilter := notion_utils.GetAllText(&properties, "Account")
		sumReduceIncome := c.dashboardStatisticsSumFilter(
			property.Date,
			"Доход",
			categoryFilter,
			accountFilter,
		)
		_, err = c.NotionClient.UpdatePage(context.Background(), row.ID, notion.UpdatePageParams{
			DatabasePageProperties: notion.DatabasePageProperties{
				"Сумма дохода": notion.DatabasePageProperty{
					Type:   notion.DBPropTypeNumber,
					Number: &sumReduceIncome,
				},
			},
		})
		if err != nil {
			log.Error(
				"Произошла ошибка при вставке данных в Статистика [доходы/расходы] по колонке `Список доходов`: ",
				err,
				". sumReduceIncome=", sumReduceIncome, " записей для вставки",
			)
		}
		sumReduceExpense := c.dashboardStatisticsSumFilter(
			property.Date,
			"Расход",
			categoryFilter,
			accountFilter,
		)
		_, err = c.NotionClient.UpdatePage(context.Background(), row.ID, notion.UpdatePageParams{
			DatabasePageProperties: notion.DatabasePageProperties{
				"Сумма расходов": notion.DatabasePageProperty{
					Type:   notion.DBPropTypeNumber,
					Number: &sumReduceExpense,
				},
			},
		})
		if err != nil {
			log.Error(
				"Произошла ошибка при вставке данных в Статистика [доходы/расходы] по колонке `Список расходов`:",
				err,
				". sumReduceExpense= ", sumReduceExpense, " записей для вставки",
			)
		}
	}
	return "Синхронизация дешборда Статистика [доходы/расходы] завершена"
}

func (c *NotionIncomeExpensesSyncUC) dashboardStatisticsSumFilter(
	dateFilter *notion.Date,
	typeFilter string,
	categoryFilter string,
	accountFilter string,
) float64 {
	filter := []notion.DatabaseQueryFilter{
		{
			Property: "Дата",
			DatabaseQueryPropertyFilter: notion.DatabaseQueryPropertyFilter{
				Date: &notion.DatePropertyFilter{
					OnOrBefore: &dateFilter.End.Time,
				},
			},
		},
		{
			Property: "Дата",
			DatabaseQueryPropertyFilter: notion.DatabaseQueryPropertyFilter{
				Date: &notion.DatePropertyFilter{
					OnOrAfter: &dateFilter.Start.Time,
				},
			},
		},
		{
			Property: "Тип",
			DatabaseQueryPropertyFilter: notion.DatabaseQueryPropertyFilter{
				Select: &notion.SelectDatabaseQueryFilter{
					Equals: typeFilter,
				},
			},
		},
		{
			Property: "Account",
			DatabaseQueryPropertyFilter: notion.DatabaseQueryPropertyFilter{
				Select: &notion.SelectDatabaseQueryFilter{
					Equals: accountFilter,
				},
			},
		},
	}
	if len(categoryFilter) > 0 {
		filter = append(
			filter,
			notion.DatabaseQueryFilter{
				Property: "Категория",
				DatabaseQueryPropertyFilter: notion.DatabaseQueryPropertyFilter{
					Select: &notion.SelectDatabaseQueryFilter{
						Equals: categoryFilter,
					},
				},
			},
		)
	}
	pages := notion_utils.AllFieldsFilterExecute(
		c.NotionClient,
		c.Conf.IncomeExpensesPageID,
		&notion.DatabaseQueryFilter{
			And: filter,
		},
		[]notion.DatabaseQuerySort{},
	)
	sumReduce := funk.Reduce(funk.Map(pages, func(page notion.Page) float64 {
		properties := page.Properties.(notion.DatabasePageProperties)
		if properties == nil {
			return 0.00
		}
		return notion_utils.GetAllNumber(&properties, "Сумма")
	}), '+', float64(0)).(float64)
	return sumReduce
}

// syncGenerateStatistic - функция генерации автоматического дешборда для Статистика [доходы/расходы]
func (c *NotionIncomeExpensesSyncUC) syncGenerateStatistic() string {
	statSyncAll := 0
	statSyncError := 0
	// Получение диапазона StartDate, EndDate
	// Текущая дата и время
	now := time.Now()
	// Начало текущего месяца
	notionStartDate := notion.NewDateTime(
		time.Date(now.Year(), now.Month(), 1, 0, 0, 0, 0, now.Location()),
		false,
	)
	notionEndDate := notion.NewDateTime(
		notionStartDate.Time.AddDate(0, 1, -1).Add(time.Hour*23+time.Minute*59+time.Second*59),
		false,
	)
	notionDate := notion.Date{
		Start: notionStartDate,
		End:   &notionEndDate,
	}
	// Удаление автоматически созданных данных в Статистика [доходы/расходы]
	automateToggle := true
	pages := notion_utils.AllFieldsFilterExecute(
		c.NotionClient,
		c.Conf.DashboardStatisticPageID,
		&notion.DatabaseQueryFilter{
			Property: "Автоматически",
			DatabaseQueryPropertyFilter: notion.DatabaseQueryPropertyFilter{
				Checkbox: &notion.CheckboxDatabaseQueryFilter{
					Equals: &automateToggle,
				},
			},
		},
		[]notion.DatabaseQuerySort{},
	)
	for _, page := range pages {
		_, _ = c.NotionClient.UpdatePage(
			context.Background(),
			page.ID,
			notion.UpdatePageParams{
				Archived: &automateToggle,
			},
		)
	}
	// Получение актуальных категорий для автоматического добавления в дешборд
	rows, err := c.NotionClient.QueryDatabase(
		context.Background(),
		c.Conf.GeneratorDashboardStatisticPageID,
		&notion.DatabaseQuery{
			PageSize: NotionPageMaxSize,
		},
	)
	if err != nil {
		message := fmt.Sprintf(
			"Произошла ошибка при получении данных из Генератор.Статистика [доходы/расходы] %+v",
			err,
		)
		log.Error(message)
		return message
	}
	for _, row := range rows.Results {
		properties := row.Properties.(notion.DatabasePageProperties)
		if properties == nil {
			continue
		}
		category := notion_utils.GetAllText(&properties, "Категория")
		pageName := "—————————"
		if len(category) > 0 {
			pageName = category
		}
		limitOfMonth := notion_utils.GetAllNumber(&properties, "Лимит на месяц")
		_, err = c.NotionClient.CreatePage(
			context.Background(),
			notion.CreatePageParams{
				ParentType: notion.ParentTypeDatabase,
				ParentID:   c.Conf.DashboardStatisticPageID,
				DatabasePageProperties: &notion.DatabasePageProperties{
					"ID": notion.DatabasePageProperty{
						Type: notion.DBPropTypeTitle,
						Title: []notion.RichText{
							{
								Type: notion.RichTextTypeText,
								Text: &notion.Text{
									Content: pageName,
								},
							},
						},
					},
					"Дата": notion.DatabasePageProperty{
						Type: notion.DBPropTypeDate,
						Date: &notionDate,
					},
					"Категория": notion.DatabasePageProperty{
						RichText: []notion.RichText{
							{
								Type: notion.RichTextTypeText,
								Text: &notion.Text{
									Content: category,
								},
							},
						},
					},
					"Автоматически": notion.DatabasePageProperty{
						Checkbox: &automateToggle,
					},
					"Лимит расходов": notion.DatabasePageProperty{
						Number: &limitOfMonth,
					},
					"Account": notion.DatabasePageProperty{
						RichText: []notion.RichText{
							{
								Type: notion.RichTextTypeText,
								Text: &notion.Text{
									Content: "RUB",
								},
							},
						},
					},
				},
			},
		)
		if err != nil {
			log.Error("Ошибка при вставке данных по категории: ", category, " с ошибкой: ", err)
			statSyncError += 1
		}
		statSyncAll += 1
	}
	return fmt.Sprintf(
		"Генерация данных для дешборда Статистика [доходы/расходы] завершена. Всего: %d, Ошибок: %d",
		statSyncAll,
		statSyncError,
	)
}

func (c *NotionIncomeExpensesSyncUC) Execute(operation IncomeExpensesSync) string {
	message := ""
	if operation == IncomeExpensesSyncAll || operation == IncomeExpensesSyncMoney {
		message += c.syncMoney()
		message += "\n"
	}
	if operation == IncomeExpensesSyncAll || operation == IncomeExpensesSyncStatistic {
		message += c.syncStatistic()
		message += "\n"
	}
	if operation == IncomeExpensesSyncGenerateStatistic {
		message += c.syncGenerateStatistic()
		message += "\n"
	}
	if len(message) <= 0 {
		return "Произошла ошибка при проведении операции"
	}
	return message
}
