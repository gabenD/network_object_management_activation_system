package useCase

import (
	"context"
	"nomas_shared/pkg/notion_utils"

	"github.com/dstotijn/go-notion"
	log "github.com/sirupsen/logrus"
)

type NotionOperation struct {
	ID            string
	OperationDate string // date-iso-format
	CardNumber    string
	TotalAmount   float64
	Category      string
	Description   string
	Bank          string
}

const (
	NotionOperationBankTinkoff = "Tinkoff"
)

type NotionAddTinkoffFileUC struct {
	NotionClient *notion.Client
	Conf         *notion_utils.NotionApiConf
}

func (c *NotionAddTinkoffFileUC) Execute(operations []NotionOperation) int {
	errors := make([]error, 0)
	for _, operation := range operations {
		rows, err := c.NotionClient.QueryDatabase(
			context.Background(),
			c.Conf.OperationPageID,
			&notion.DatabaseQuery{
				Filter: &notion.DatabaseQueryFilter{
					Property: "ID",
					DatabaseQueryPropertyFilter: notion.DatabaseQueryPropertyFilter{
						Title: &notion.TextPropertyFilter{
							Equals: operation.ID,
						},
					},
				},
				PageSize: 1,
			},
		)
		if err != nil {
			log.Info("Не удалось загрузить операцию из БД всех операций. ID: ", operation.ID, "\nОшибка: ", err.Error())
			errors = append(errors, err)
			continue
		}
		if len(rows.Results) > 0 {
			log.Info("Операция ID: ", operation.ID, " уже существует")
			continue
		}
		paymentDate, err := notion.ParseDateTime(operation.OperationDate)
		if err != nil {
			log.Info("Не смогли перевести дату операции в datetime объект ID: ", operation.ID)
			errors = append(errors, err)
			continue
		}
		_, err = c.NotionClient.CreatePage(context.Background(), notion.CreatePageParams{
			ParentType: notion.ParentTypeDatabase,
			ParentID:   c.Conf.OperationPageID,
			DatabasePageProperties: &notion.DatabasePageProperties{
				"ID": notion.DatabasePageProperty{
					Type: notion.DBPropTypeTitle,
					Title: []notion.RichText{
						{
							Type: notion.RichTextTypeText,
							Text: &notion.Text{
								Content: operation.ID,
							},
						},
					},
				},
				"Дата операции": notion.DatabasePageProperty{
					Type: notion.DBPropTypeDate,
					Date: &notion.Date{
						Start: paymentDate,
					},
				},
				"Номер карты": notion.DatabasePageProperty{
					Type: notion.DBPropTypeRichText,
					RichText: []notion.RichText{
						{
							Type: notion.RichTextTypeText,
							Text: &notion.Text{
								Content: operation.CardNumber,
							},
						},
					},
				},
				"Сумма операции": notion.DatabasePageProperty{
					Type:   notion.DBPropTypeNumber,
					Number: &operation.TotalAmount,
				},
				"Категория": notion.DatabasePageProperty{
					Type: notion.DBPropTypeRichText,
					RichText: []notion.RichText{
						{
							Type: notion.RichTextTypeText,
							Text: &notion.Text{
								Content: operation.Category,
							},
						},
					},
				},
				"Описание": notion.DatabasePageProperty{
					Type: notion.DBPropTypeRichText,
					RichText: []notion.RichText{
						{
							Type: notion.RichTextTypeText,
							Text: &notion.Text{
								Content: operation.Description,
							},
						},
					},
				},
				"Банк": notion.DatabasePageProperty{
					Type: notion.DBPropTypeRichText,
					RichText: []notion.RichText{
						{
							Type: notion.RichTextTypeText,
							Text: &notion.Text{
								Content: operation.Bank,
							},
						},
					},
				},
			},
		})
		if err != nil {
			log.Info("Не создалась операция ID: ", operation.ID, "\nОшибка: ", err.Error())
			errors = append(errors, err)
			continue
		}
	}
	return len(errors)
}

func (c *NotionAddTinkoffFileUC) pagesToSetIDS(pages *[]notion.Page) map[string]bool {
	set := make(map[string]bool)
	for _, page := range *pages {
		properties := page.Properties.(notion.DatabasePageProperties)
		if properties == nil {
			continue
		}
		set[notion_utils.GetAllText(&properties, "ID")] = true
	}
	return set
}
