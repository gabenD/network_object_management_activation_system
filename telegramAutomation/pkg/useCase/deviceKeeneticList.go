package useCase

import (
	"cmp"
	"crypto/hmac"
	"crypto/sha1"
	"encoding/hex"
	"fmt"
	"nomas_shared/pkg/filter"
	"nomas_shared/pkg/keenetic_client"
	"slices"
	"strings"

	"github.com/thoas/go-funk"
)

type DeviceKeeneticUseCase struct {
	KeeneticClient *keenetic_client.KeeneticClient
}

func (c *DeviceKeeneticUseCase) Execute(hashMac string) string {
	response := c.wakeOnDevice(hashMac)
	if response == "" {
		response = c.listDeviceClient()
	}
	if response == "" {
		response = "Произошла ошибка при отправке сообщения ⌛️"
	}
	return response
}

func (c *DeviceKeeneticUseCase) listDeviceClient() string {
	kl, err := c.KeeneticClient.List()
	if err != nil {
		return fmt.Sprintf("Ошибка Keeneric %+v", err)
	}
	slices.SortFunc(kl.Host, func(a, b keenetic_client.KeenHotspotResponseItem) int {
		return cmp.Or(
			cmp.Compare(filter.Bool2Int(a.Registered), filter.Bool2Int(b.Registered)),
			cmp.Compare(c.chooseName(a.Name, a.Hostname, a.Ip), c.chooseName(b.Name, b.Hostname, b.Ip)),
		)
	})

	result := make([]string, 0)
	for index, device := range kl.Host {
		suffixDeviceInfo := "🟥"
		if device.Active {
			suffixDeviceInfo = "🟩"
		}
		segmentInfo := ""
		if device.Interface != nil && len(device.Interface.Description) > 0 {
			segmentInfo = fmt.Sprintf(" (%s)", device.Interface.Description)
		}
		deviceInfo := fmt.Sprintf(
			"%s [%d] %s %s /deviceWakeOn_%s",
			suffixDeviceInfo,
			index+1,
			c.chooseName(device.Name, device.Hostname, device.Ip),
			segmentInfo,
			c.macAddressToString(device.Mac),
		)
		result = append(result, deviceInfo)
	}
	return strings.Join(result, "\n")
}

func (c *DeviceKeeneticUseCase) wakeOnDevice(hashMac string) string {
	if len(hashMac) == 0 {
		return ""
	}
	kl, err := c.KeeneticClient.List()
	if err != nil {
		return fmt.Sprintf("Ошибка Keenetic %+v", err)
	}

	devices := funk.Filter(kl.Host, func(device keenetic_client.KeenHotspotResponseItem) bool {
		return c.compareHashAndMac(device.Mac, hashMac)
	}).([]keenetic_client.KeenHotspotResponseItem)
	if len(devices) == 0 {
		return "Клиент не найден 😔"
	}
	if len(devices) > 1 {
		return "Клиентов несколько 😲"
	}
	device := devices[0]
	err = c.KeeneticClient.Wake(device.Mac)
	if err != nil {
		return fmt.Sprintf("Ошибка пробуждения клиента из Keenetic %+v", err)
	}
	return fmt.Sprintf(
		"Разубили клиента %s 🍽",
		c.chooseName(device.Name, device.Hostname, device.Ip),
	)
}

func (c *DeviceKeeneticUseCase) chooseName(names ...string) string {
	for _, name := range names {
		if len(name) > 0 {
			return name
		}
	}
	return ""
}

func (c *DeviceKeeneticUseCase) macAddressToString(mac string) string {
	key := []byte(nil)
	h := hmac.New(sha1.New, key)
	h.Write([]byte(mac))
	return hex.EncodeToString(h.Sum(nil))
}

func (c *DeviceKeeneticUseCase) compareHashAndMac(mac string, hashMac string) bool {
	return c.macAddressToString(mac) == hashMac
}
