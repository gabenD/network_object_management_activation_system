package useCase

import (
	"fmt"
	"nomas_shared/pkg/metabase_sync_client"
)

type MetabaseSyncUseCase struct {
	MetabaseClient metabase_sync_client.INewMetabaseSyncClient
}

func (c *MetabaseSyncUseCase) Execute() string {
	message := ""
	respOne, errOne := c.MetabaseClient.IncomeExpense("2000-01-01", "2100-01-01")
	if errOne != nil {
		message += fmt.Sprintf("Произошка ошибка при синхронизации MetabaseDashborad %+v\n", errOne)
	}
	message += fmt.Sprintf("Ответ MetabaseDashborad.IncomeExpense: %v\n", respOne.Message)
	respTwo, errTwo := c.MetabaseClient.CurrencyLimit()
	if errTwo != nil {
		message += fmt.Sprintf("Произошка ошибка при синхронизации MetabaseDashborad %+v\n", errTwo)
	}
	message += fmt.Sprintf("Ответ MetabaseDashborad.CurrencyLimit: %v\n", respTwo.Message)
	return message
}
