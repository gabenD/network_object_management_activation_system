package useCase

import (
	"fmt"
	"math"
	"net/http"
	"nomas_telegram_wol/pkg/structParser"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

type TinkoffSyncFileUseCase struct {
	BotAPI       *tgbotapi.BotAPI
	NotionSyncUC *NotionAddTinkoffFileUC
}

func (c *TinkoffSyncFileUseCase) Execute(id string) string {
	fileURL, err := c.BotAPI.GetFileDirectURL(id)
	if err != nil {
		return fmt.Sprintf("Произошла ошибка при скачивании файла: %+v", err)
	}
	response, err := http.Get(fileURL)
	if err != nil {
		return fmt.Sprintf("Не создана ссылка для скачивания файла, попробуйте позже: %+v", err)
	}
	defer response.Body.Close()
	payload, err := structParser.NewTinkoffTransactionItem(response.Body)
	if err != nil {
		return fmt.Sprintf("Не получилось распарсить данные файла: %+v", err)
	}
	notionOperations := []NotionOperation{}
	for _, operation := range payload {
		if operation.Status != "OK" {
			continue
		}
		notionOperations = append(
			notionOperations,
			NotionOperation{
				ID:            operation.ID(),
				OperationDate: operation.OperationDate.Format("2006-01-02"),
				CardNumber:    operation.CardNumber,
				TotalAmount:   operation.TotalAmount.Float64() * operation.PaymentAmount.Float64() / math.Abs(operation.PaymentAmount.Float64()),
				Category:      operation.Category,
				Description:   operation.Description,
				Bank:          NotionOperationBankTinkoff,
			},
		)
	}
	if len(notionOperations) == 0 {
		return "Нет доступных операций"
	}
	countErrors := c.NotionSyncUC.Execute(notionOperations)
	return fmt.Sprintf("Кол-во ошибок возникших в процессе обработке файла: %+v", countErrors)
}
