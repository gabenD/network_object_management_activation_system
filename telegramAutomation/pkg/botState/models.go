package botState

type StructUserState struct {
	Module string `json:"module"`
}

type StructUserAndState struct {
	Id    string           `json:"id"`
	State *StructUserState `json:"state"`
}

type StructUsersState struct {
	Users []*StructUserAndState `json:"users"`
}
