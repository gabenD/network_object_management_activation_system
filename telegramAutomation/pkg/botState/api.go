package botState

import (
	"encoding/json"
	"os"

	log "github.com/sirupsen/logrus"
)

const FileBotState = "botState.local.json"

func LoadBotState() *StructUsersState {
	byteValue, err := os.ReadFile(FileBotState)
	if err != nil {
		log.Debug("Попытка открыть файл для сохранения состояния бота не успешная ", err)
		return &StructUsersState{}
	}
	var entities StructUsersState
	err = json.Unmarshal(byteValue, &entities)
	if err != nil {
		log.Debug("Попытка распарсить данные для сохранения состояния бота не успешная ", err)
	}
	return &entities
}

func SaveBotState(entities *StructUsersState) error {
	entitiesJson, err := json.Marshal(entities)
	if err != nil {
		log.Debug("Попытка сохранить данные для сохранения состояния бота не успешная ", err)
	}
	return os.WriteFile(FileBotState, entitiesJson, 0644)
}

func (c *StructUsersState) Get(userId string) *StructUserState {
	defaultState := &StructUserState{}
	if c.Users == nil {
		return defaultState
	}
	for _, user := range c.Users {
		if user.Id == userId && user.State != nil {
			return user.State
		}
	}
	return defaultState
}

func (c *StructUsersState) Upsert(userId string, state *StructUserState) {
	if c.Users == nil {
		c.Users = make([]*StructUserAndState, 0)
	}
	for _, user := range c.Users {
		if user.Id == userId {
			log.Debug("Обновили существующего пользователя в dbState ", userId)
			user.State = state
			return
		}
	}
	log.Debug("Добавляем нового пользователя в dbState ", userId)
	c.Users = append(c.Users, &StructUserAndState{
		Id:    userId,
		State: state,
	})
}
