package configurator

import (
	"nomas_shared/pkg/sber_client"
)

type IotDeviceConf struct {
	LongPollingDeviceId  string `json:"lp_device_id"`
	LongPollingToken     string `json:"lp_token"`
	SberDeviceId         string `json:"sber_device_id"`
	SberScenarioId       string `json:"sber_scenario_id"`
	SberAccountId        string `json:"sber_account_id"`
	FirstStateFromServer bool   `json:"first_state_from_server"`
	Type                 string `json:"type"`
}

type IotConf struct {
	BaseUrl           string          `json:"url"`
	MaxTimeoutSeconds int64           `json:"max_timeout_seconds"`
	Devices           []IotDeviceConf `json:"devices"`
}

type Conf struct {
	Debug             bool                                `json:"debug"`
	AuthorizationSber sber_client.AuthorizationClientConf `json:"auth_sber"`
	IoT               IotConf                             `json:"iot"`
}
