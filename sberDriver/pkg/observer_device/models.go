package observer_device

type ModelAnyOptions struct {
	LastSetMethod string `json:"last_set_method"`
}
type ModelRGBLamp struct {
	Power      ModelRGBLampPowerValue      `json:"power"`
	Brightness ModelRGBLampBrightnessValue `json:"brightness"`
	Color      ModelRGBLampColorValue      `json:"color"`
	Options    ModelAnyOptions             `json:"_options"`
}

type ModelRGBLampPowerValue struct {
	Value int `json:"value"  validate:"min=0,max=1"`
}

type ModelRGBLampBrightnessValue struct {
	Value float64 `json:"value"  validate:"min=0,max=100"`
}

type ModelRGBLampColorValue struct {
	Color       int64  `json:"color"`
	Temperature int64  `json:"temperature" validate:"min=2700,max=6500"`
	Type        string `json:"type" validate:"oneof=temperature rgb"`
	Red         int    `json:"r"  validate:"min=0,max=255"`
	Green       int    `json:"g"  validate:"min=0,max=255"`
	Blue        int    `json:"b"  validate:"min=0,max=255"`
	Hex         string `json:"#rgb"`
}

type ModelTV struct {
	Power   ModelTVPowerValue  `json:"power"`
	Volume  ModelTVVolumeValue `json:"volume"`
	Mute    ModelTVMuteValue   `json:"mute"`
	Options ModelAnyOptions    `json:"_options"`
}

type ModelTVPowerValue struct {
	Value int `json:"value"  validate:"min=0,max=1"`
}

type ModelTVVolumeValue struct {
	Value int `json:"value"  validate:"min=0,max=1000"`
}

type ModelTVMuteValue struct {
	Value int `json:"value"  validate:"min=0,max=1"`
}
