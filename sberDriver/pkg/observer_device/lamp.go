package observer_device

import (
	"nomas_sber_driver/pkg/configurator"
	"nomas_shared/pkg/colorful_utils"
	"nomas_shared/pkg/long_polling_client"
	"nomas_shared/pkg/sber_client"

	"github.com/lucasb-eyer/go-colorful"
	log "github.com/sirupsen/logrus"
)

func CreateLampObserverDevice(
	sberDriverClient *sber_client.ActivationSberClient,
	clientConf *configurator.IotConf,
	deviceConf *configurator.IotDeviceConf,
	debug *bool,
) {
	lpConfig := long_polling_client.Conf{
		BaseUrl:              clientConf.BaseUrl,
		MaxTimeoutSeconds:    clientConf.MaxTimeoutSeconds,
		DeviceId:             deviceConf.LongPollingDeviceId,
		FirstStateFromServer: deviceConf.FirstStateFromServer,
		Token:                deviceConf.LongPollingToken,
	}
	lamp := sber_client.NewRGBLamp(
		deviceConf.SberDeviceId,
		sberDriverClient,
	)
	callbacks := []func(
		*ModelRGBLamp,
		*long_polling_client.LongPollingClient,
	){func(
		device *ModelRGBLamp,
		ctx *long_polling_client.LongPollingClient,
	) {
		switch device.Options.LastSetMethod {
		case "power":
			// Выключение
			if device.Power.Value == 0 {
				_ = lamp.Off()
				return
			}
			// Включение
			if device.Power.Value == 1 {
				_ = lamp.On()
			}
		case "color", "brightness":
			// Вычисляем текущую яркость
			bright := colorful_utils.LinearValueMap(
				int64(device.Brightness.Value),
				0,
				100,
				50,
				1000,
			)

			// Установка цвета
			if device.Color.Type == "rgb" && device.Color.Hex != "" {
				color, err := colorful.Hex(device.Color.Hex)
				if err != nil {
					log.Warning("Callback device papse color error", err)
				} else {
					h, s, _ := color.Hsv()
					_ = lamp.Color(int64(h), int64(s*1000), bright)
				}
			}

			// Установка уровня освещения
			if device.Color.Type == "temperature" && device.Color.Temperature >= 2700 && device.Color.Temperature <= 6500 {
				temp := colorful_utils.LinearValueMap(
					device.Color.Temperature,
					2700,
					6500,
					0,
					1000,
				)
				_ = lamp.Temperature(temp, bright)
			}
		}
	}}
	devicePolling, err := NewModelRGBLampObserver(
		&lpConfig,
		debug,
		&callbacks,
	)
	if err != nil {
		log.Panic(err)
	}
	defer devicePolling.Shutdown()
	go devicePolling.RunForever()
}
