package observer_device

import (
	"nomas_sber_driver/pkg/configurator"
	"nomas_shared/pkg/long_polling_client"
	"nomas_shared/pkg/sber_client"

	log "github.com/sirupsen/logrus"
)

func CreateTvObserverDevice(
	sberDriverClient *sber_client.ActivationSberClient,
	clientConf *configurator.IotConf,
	deviceConf *configurator.IotDeviceConf,
	debug *bool,
) {
	lpConfig := long_polling_client.Conf{
		BaseUrl:              clientConf.BaseUrl,
		MaxTimeoutSeconds:    clientConf.MaxTimeoutSeconds,
		DeviceId:             deviceConf.LongPollingDeviceId,
		FirstStateFromServer: deviceConf.FirstStateFromServer,
		Token:                deviceConf.LongPollingToken,
	}
	tv := sber_client.NewTVSalute(
		deviceConf.SberAccountId,
		deviceConf.SberScenarioId,
		deviceConf.SberDeviceId,
		sberDriverClient,
	)
	callbacks := []func(
		*ModelTV,
		*long_polling_client.LongPollingClient,
	){func(
		device *ModelTV,
		ctx *long_polling_client.LongPollingClient,
	) {
		switch device.Options.LastSetMethod {
		case "power":
			// Выключение
			if device.Power.Value == 0 {
				_ = tv.Off()
				return
			}
			// Включение
			if device.Power.Value == 1 {
				_ = tv.On()
			}
		case "mute":
			// Управление громкостью
			if device.Mute.Value == 1 {
				_ = tv.Mute()
			} else {
				_ = tv.SetVolume(device.Volume.Value / 100)
			}
		case "volume":
			_ = tv.SetVolume(device.Volume.Value / 100)

		}
	}}
	devicePolling, err := NewModelTVObserver(
		&lpConfig,
		debug,
		&callbacks,
	)
	if err != nil {
		log.Panic(err)
	}
	defer devicePolling.Shutdown()
	go devicePolling.RunForever()
}
