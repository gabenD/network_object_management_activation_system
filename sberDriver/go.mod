module nomas_sber_driver

go 1.21

require (
	github.com/lucasb-eyer/go-colorful v1.0.2
	github.com/sirupsen/logrus v1.9.3
)

require (
	github.com/stretchr/testify v1.8.4 // indirect
	golang.org/x/sys v0.13.0 // indirect
)
