package main

import (
	"nomas_sber_driver/pkg/configurator"
	"nomas_sber_driver/pkg/observer_device"
	"nomas_shared/pkg/load_configurator"
	"nomas_shared/pkg/sber_client"
	"time"

	log "github.com/sirupsen/logrus"
)

func main() {
	conf, err := load_configurator.LoadConfigFromArguments[configurator.Conf]()
	if err != nil {
		log.Panic(err)
	}
	load_configurator.SetLogger(&conf.Debug)

	sberDriverClient := sber_client.NewActivationSberClient(
		&conf.AuthorizationSber,
		&conf.Debug,
	)
	for _, virtualDevice := range conf.IoT.Devices {
		switch virtualDevice.Type {
		case "lamp":
			observer_device.CreateLampObserverDevice(
				sberDriverClient,
				&conf.IoT,
				&virtualDevice,
				&conf.Debug,
			)
		case "tv":
			observer_device.CreateTvObserverDevice(
				sberDriverClient,
				&conf.IoT,
				&virtualDevice,
				&conf.Debug,
			)
		}
	}
	// Sleep forever
	for {
		time.Sleep(1138800 * time.Hour)
	}
}
