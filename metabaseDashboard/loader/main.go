package main

import (
	"fmt"
	"io"
	"nomas_metabase_dashboard/pkg/repository/database"
	"os"

	"ariga.io/atlas-provider-gorm/gormschema"
)

func main() {
	// usage on `atlas migrate diff --env gorm`
	// usage on `atlas migrate apply --url "$POSTGRES_URL"`
	stmts, err := gormschema.New("postgres").Load(
		&database.IncomeExpenseNotion{},
		&database.CurrencyLimitNotion{},
	)
	if err != nil {
		fmt.Fprintf(os.Stderr, "failed to load gorm schema: %v\n", err)
		os.Exit(1)
	}
	io.WriteString(os.Stdout, stmts)
}
