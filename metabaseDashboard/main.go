package main

import (
	"nomas_metabase_dashboard/pkg/configurator"
	"nomas_metabase_dashboard/pkg/repository/database"
	"nomas_metabase_dashboard/server"
	"nomas_shared/pkg/load_configurator"

	log "github.com/sirupsen/logrus"
)

func main() {
	conf, err := load_configurator.LoadConfigFromArguments[configurator.MetabaseDashboardServerConf]()
	if err != nil {
		log.Panic(err)
	}
	load_configurator.SetLogger(&conf.Debug)
	deps := server.NewDependency(conf)
	err = database.Migrate(&deps.Database)
	if err != nil {
		log.Panic(err)
	}
	fiberApp := server.NewFiberServer(deps)
	log.Info("Starting server")
	fiberApp()
}
