-- Modify "income_expense_notions" table
ALTER TABLE "public"."income_expense_notions" ADD COLUMN "epic" text NULL, ADD COLUMN "account" text NULL;
-- Create index "idx_income_expense_notions_account" to table: "income_expense_notions"
CREATE INDEX "idx_income_expense_notions_account" ON "public"."income_expense_notions" ("account");
-- Create index "idx_income_expense_notions_epic" to table: "income_expense_notions"
CREATE INDEX "idx_income_expense_notions_epic" ON "public"."income_expense_notions" ("epic");
