-- Create "currency_limit_notions" table
CREATE TABLE "public"."currency_limit_notions" (
  "id" bigserial NOT NULL,
  "created_at" timestamptz NULL,
  "updated_at" timestamptz NULL,
  "date" timestamptz NULL,
  "category" text NULL,
  "amount" numeric NULL,
  "soft_lock_deleted_at" timestamptz NULL,
  PRIMARY KEY ("id")
);
-- Create index "idx_currency_limit_notions_category" to table: "currency_limit_notions"
CREATE INDEX "idx_currency_limit_notions_category" ON "public"."currency_limit_notions" ("category");
-- Create index "idx_currency_limit_notions_date" to table: "currency_limit_notions"
CREATE INDEX "idx_currency_limit_notions_date" ON "public"."currency_limit_notions" ("date");
-- Create index "idx_currency_limit_notions_soft_lock_deleted_at" to table: "currency_limit_notions"
CREATE INDEX "idx_currency_limit_notions_soft_lock_deleted_at" ON "public"."currency_limit_notions" ("soft_lock_deleted_at");
