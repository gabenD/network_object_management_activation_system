-- Create "income_expense_notions" table
CREATE TABLE "public"."income_expense_notions" (
  "id" bigserial NOT NULL,
  "created_at" timestamptz NULL,
  "updated_at" timestamptz NULL,
  "external_id" text NULL,
  "type" text NULL,
  "date" timestamptz NULL,
  "category" text NULL,
  "name" text NULL,
  "amount" numeric NULL,
  "taxon" text NULL,
  "soft_lock_deleted_at" timestamptz NULL,
  PRIMARY KEY ("id")
);
-- Create index "idx_income_expense_notions_category" to table: "income_expense_notions"
CREATE INDEX "idx_income_expense_notions_category" ON "public"."income_expense_notions" ("category");
-- Create index "idx_income_expense_notions_date" to table: "income_expense_notions"
CREATE INDEX "idx_income_expense_notions_date" ON "public"."income_expense_notions" ("date");
-- Create index "idx_income_expense_notions_external_id" to table: "income_expense_notions"
CREATE UNIQUE INDEX "idx_income_expense_notions_external_id" ON "public"."income_expense_notions" ("external_id");
-- Create index "idx_income_expense_notions_soft_lock_deleted_at" to table: "income_expense_notions"
CREATE INDEX "idx_income_expense_notions_soft_lock_deleted_at" ON "public"."income_expense_notions" ("soft_lock_deleted_at");
-- Create index "idx_income_expense_notions_taxon" to table: "income_expense_notions"
CREATE INDEX "idx_income_expense_notions_taxon" ON "public"."income_expense_notions" ("taxon");
-- Create index "idx_income_expense_notions_type" to table: "income_expense_notions"
CREATE INDEX "idx_income_expense_notions_type" ON "public"."income_expense_notions" ("type");
