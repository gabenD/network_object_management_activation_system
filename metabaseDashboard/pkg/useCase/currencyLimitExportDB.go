package useCase

import (
	"fmt"
	"nomas_metabase_dashboard/pkg/repository/database"
	"nomas_shared/pkg/notion_utils"
	"time"

	"github.com/dstotijn/go-notion"
	log "github.com/sirupsen/logrus"
	"github.com/thoas/go-funk"
)

type NotionCurrencyLimitSyncToDBUC struct {
	NotionClient *notion.Client
	Conf         *notion_utils.NotionApiConf
	DBConnect    *database.Connect
}

type NotionCurrencyLimitSyncToDBUCInputDTO struct{}

func (c *NotionCurrencyLimitSyncToDBUC) FirstOfMonth() time.Time {
	now := time.Now()
	firstOfMonth := time.Date(now.Year(), now.Month(), 1, 0, 0, 0, 0, now.Location())
	log.Debug("Первый день текущего месяца: ", firstOfMonth)
	return firstOfMonth
}

type NotionCurrencyLimitSyncToDBOutputDTO struct {
	Message string `json:"message"`
}

func (c *NotionCurrencyLimitSyncToDBUC) Execute(
	_ NotionCurrencyLimitSyncToDBUCInputDTO,
) NotionCurrencyLimitSyncToDBOutputDTO {
	database.CurrencyLimitNotionDeletedAtByDate(c.DBConnect, c.FirstOfMonth())
	models := c.getNotionCurrencyLimit()
	errors := 0
	for _, model := range models {
		_, err := database.CurrencyLimitNotionUpdateOrCreate(c.DBConnect, &model)
		if err != nil {
			log.Error("При обработке NotionIncomeExpenseSyncToDBUC произошла ошибка ", err)
			errors += 1
		}
	}
	database.CurrencyLimitNotionDeleteOnSoftDeleted(c.DBConnect)
	return NotionCurrencyLimitSyncToDBOutputDTO{
		Message: fmt.Sprintf(
			"Успешная синхронизация списка моделей из Генератор доходов и расходов в БД %d шт; ошибок %d",
			len(models),
			errors,
		),
	}
}

func (c *NotionCurrencyLimitSyncToDBUC) notionToDbModel(page notion.Page) database.CurrencyLimitNotion {
	properties := page.Properties.(notion.DatabasePageProperties)
	if properties == nil {
		log.Debug("[notionToDbModel] На нашли properties в ", page.ID)
		return database.CurrencyLimitNotion{}
	}
	return database.CurrencyLimitNotion{
		Date:     c.FirstOfMonth(),
		Category: notion_utils.GetAllText(&properties, "Категория"),
		Amount:   notion_utils.GetAllNumber(&properties, "Лимит на месяц"),
	}
}

// getNotionCurrencyLimit - синхронизации дешборда Генератор.Статистика [доходы/расходы]
func (c *NotionCurrencyLimitSyncToDBUC) getNotionCurrencyLimit() []database.CurrencyLimitNotion {
	pages := notion_utils.AllFieldsFilterExecute(
		c.NotionClient,
		c.Conf.GeneratorDashboardStatisticPageID,
		&notion.DatabaseQueryFilter{
			Property: "Категория",
			DatabaseQueryPropertyFilter: notion.DatabaseQueryPropertyFilter{
				Select: &notion.SelectDatabaseQueryFilter{
					IsNotEmpty: true,
				},
			},
		},
		[]notion.DatabaseQuerySort{},
	)
	models := funk.Map(pages, c.notionToDbModel).([]database.CurrencyLimitNotion)
	return models
}
