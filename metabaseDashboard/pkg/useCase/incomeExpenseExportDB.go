package useCase

import (
	"fmt"
	"nomas_metabase_dashboard/pkg/repository/database"
	"nomas_shared/pkg/notion_utils"
	"time"

	"github.com/dstotijn/go-notion"
	log "github.com/sirupsen/logrus"
	"github.com/thoas/go-funk"
)

type NotionIncomeExpenseSyncToDBUC struct {
	NotionClient *notion.Client
	Conf         *notion_utils.NotionApiConf
	DBConnect    *database.Connect
}

type NotionIncomeExpenseSyncToDBInputDTO struct {
	At string `json:"at"`
	To string `json:"to"`
}

func (c *NotionIncomeExpenseSyncToDBInputDTO) AtTime() time.Time {
	date, _ := time.Parse("2006-01-02", c.At)
	return date
}

func (c *NotionIncomeExpenseSyncToDBInputDTO) ToTime() time.Time {
	date, _ := time.Parse("2006-01-02", c.To)
	return date
}

type NotionIncomeExpenseSyncToDBOutputDTO struct {
	Message string `json:"message"`
}

func (c *NotionIncomeExpenseSyncToDBUC) Execute(
	input NotionIncomeExpenseSyncToDBInputDTO,
) NotionIncomeExpenseSyncToDBOutputDTO {
	database.IncomeExpenseNotionSetDeletedAtByCreated(c.DBConnect, input.AtTime(), input.ToTime())
	models := c.getNotionIncomeExpenseData(input.AtTime(), input.ToTime())
	errors := 0
	for _, model := range models {
		_, err := database.IncomeExpenseNotionUpdateOrCreate(c.DBConnect, &model)
		if err != nil {
			log.Error("При обработке NotionIncomeExpenseSyncToDBUC произошла ошибка ", err)
			errors += 1
		}
	}
	database.IncomeExpenseNotionDeleteOnSoftDeleted(c.DBConnect)
	return NotionIncomeExpenseSyncToDBOutputDTO{
		Message: fmt.Sprintf(
			"Успешная синхронизация списка моделей из [Доходы/Расходы] в БД %d шт; ошибок %d",
			len(models),
			errors,
		),
	}
}

func (c *NotionIncomeExpenseSyncToDBUC) notionToDbModel(page notion.Page) database.IncomeExpenseNotion {
	properties := page.Properties.(notion.DatabasePageProperties)
	if properties == nil {
		log.Debug("[notionToDbModel] На нашли properties в ", page.ID)
		return database.IncomeExpenseNotion{}
	}
	return database.IncomeExpenseNotion{
		ExternalID: page.ID,
		Type:       notion_utils.GetAllText(&properties, "Тип"),
		Date:       notion_utils.GetAllDate(&properties, "Дата").Time,
		Category:   notion_utils.GetAllText(&properties, "Категория"),
		Name:       notion_utils.GetAllText(&properties, "Название"),
		Amount:     notion_utils.GetAllNumber(&properties, "Сумма"),
		Taxon:      notion_utils.GetAllText(&properties, "Таксоны"),
		Epic:       notion_utils.GetAllText(&properties, "Epic"),
		Account:    notion_utils.GetAllText(&properties, "Account"),
	}
}

// getNotionIncomeExpenseData - Экспорт из Notion
func (c *NotionIncomeExpenseSyncToDBUC) getNotionIncomeExpenseData(
	at time.Time,
	to time.Time,
) []database.IncomeExpenseNotion {
	filter := []notion.DatabaseQueryFilter{
		{
			Property: "Дата",
			DatabaseQueryPropertyFilter: notion.DatabaseQueryPropertyFilter{
				Date: &notion.DatePropertyFilter{
					OnOrBefore: &to,
				},
			},
		},
		{
			Property: "Дата",
			DatabaseQueryPropertyFilter: notion.DatabaseQueryPropertyFilter{
				Date: &notion.DatePropertyFilter{
					OnOrAfter: &at,
				},
			},
		},
	}
	pages := notion_utils.AllFieldsFilterExecute(
		c.NotionClient,
		c.Conf.IncomeExpensesPageID,
		&notion.DatabaseQueryFilter{
			And: filter,
		},
		[]notion.DatabaseQuerySort{},
	)
	models := funk.Map(pages, c.notionToDbModel).([]database.IncomeExpenseNotion)
	return models
}
