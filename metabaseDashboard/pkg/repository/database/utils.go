package database

import (
	log "github.com/sirupsen/logrus"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type Connect struct {
	Database DatabaseConf
	db       *gorm.DB
}

func (c *Connect) Connect() (*gorm.DB, error) {
	if c.db != nil {
		return c.db, nil
	}
	db, err := gorm.Open(postgres.Open(c.Database.DSN), &gorm.Config{})
	if err != nil {
		log.Error("Failed to connect to database", err)
	} else {
		c.db = db
	}
	return db, err
}
