package database

type DatabaseConf struct {
	DSN            string `json:"dsn"`
	MigrateCommand string `json:"migrate_command"`
}
