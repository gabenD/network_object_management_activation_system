package database

import (
	"os"
	"os/exec"
	"path/filepath"

	log "github.com/sirupsen/logrus"
)

func Migrate(
	ctx *Connect,
) error {
	if len(ctx.Database.MigrateCommand) <= 0 {
		return nil
	}
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		log.Fatal(err)
	}
	cmd := exec.Command(ctx.Database.MigrateCommand)
	cmd.Dir = dir
	return cmd.Start()
}
