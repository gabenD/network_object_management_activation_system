package database

import (
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"
)

type CurrencyLimitNotion struct {
	Base
	Date              time.Time `gorm:"index"`
	Category          string    `gorm:"index"`
	Amount            float64
	SoftLockDeletedAt *time.Time `gorm:"index"`
}

func CurrencyLimitNotionDeletedAtByDate(
	ctx *Connect,
	date time.Time,
) {
	db, _ := ctx.Connect()
	result := db.Model(&CurrencyLimitNotion{}).Where("date = ?", date).Update(
		"soft_lock_deleted_at",
		time.Now(),
	)
	log.Debug(fmt.Sprintf("CurrencyLimitNotionDeletedAtByDate: result: %+v", result))
}

func CurrencyLimitNotionDeleteOnSoftDeleted(
	ctx *Connect,
) {
	db, _ := ctx.Connect()
	result := db.Where("soft_lock_deleted_at is not null").Delete(&CurrencyLimitNotion{})
	log.Debug(fmt.Sprintf("CurrencyLimitNotionDeleteOnSoftDeleted: result: %+v", result))
}

func CurrencyLimitNotionUpdateOrCreate(
	ctx *Connect,
	entity *CurrencyLimitNotion,
) (*CurrencyLimitNotion, error) {
	db, _ := ctx.Connect()
	entityDB := CurrencyLimitNotion{}
	db.Where("date = ? and category = ?", entity.Date, entity.Category).Find(&entityDB)
	if entityDB.ID != 0 {
		// Update
		log.Debug(fmt.Sprintf("CurrencyLimitNotionUpdateOrCreate: entity update: %+v", entity))
		entity.ID = entityDB.ID
		entity.CreatedAt = entityDB.CreatedAt
		entity.UpdatedAt = entityDB.UpdatedAt
		entity.SoftLockDeletedAt = nil
		result := db.Save(&entity)
		return entity, result.Error
	} else {
		// Create
		log.Debug(fmt.Sprintf("CurrencyLimitNotionUpdateOrCreate: entity create: %+v", entity))
		result := db.Create(entity)
		return entity, result.Error
	}
}
