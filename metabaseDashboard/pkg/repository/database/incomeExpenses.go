package database

import (
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"
)

type IncomeExpenseNotion struct {
	Base
	ExternalID        string    `gorm:"uniqueIndex"`
	Type              string    `gorm:"index"`
	Date              time.Time `gorm:"index"`
	Category          string    `gorm:"index"`
	Name              string
	Amount            float64
	Taxon             string     `gorm:"index"`
	Epic              string     `gorm:"index"`
	Account           string     `gorm:"index"`
	SoftLockDeletedAt *time.Time `gorm:"index"`
}

func IncomeExpenseNotionSetDeletedAtByCreated(
	ctx *Connect,
	at time.Time,
	to time.Time,
) {
	db, _ := ctx.Connect()
	result := db.Model(&IncomeExpenseNotion{}).Where("created_at BETWEEN ? AND ?", at, to).Update(
		"soft_lock_deleted_at",
		time.Now(),
	)
	log.Debug(fmt.Sprintf("IncomeExpenseNotionSetDeletedAtByCreated: result: %+v", result))
}

func IncomeExpenseNotionDeleteOnSoftDeleted(
	ctx *Connect,
) {
	db, _ := ctx.Connect()
	result := db.Where("soft_lock_deleted_at is not null").Delete(&IncomeExpenseNotion{})
	log.Debug(fmt.Sprintf("IncomeExpenseNotionDeleteOnSoftDeleted: result: %+v", result))
}

func IncomeExpenseNotionUpdateOrCreate(
	ctx *Connect,
	entity *IncomeExpenseNotion,
) (*IncomeExpenseNotion, error) {
	if entity == nil || len(entity.ExternalID) <= 0 {
		return nil, nil
	}
	db, _ := ctx.Connect()
	entityDB := IncomeExpenseNotion{}
	db.Where("external_id = ?", entity.ExternalID).Find(&entityDB)
	if entityDB.ID != 0 {
		// Update
		log.Debug(fmt.Sprintf("IncomeExpenseNotionUpdateOrCreate: entity update: %+v", entity))
		entity.ID = entityDB.ID
		entity.CreatedAt = entityDB.CreatedAt
		entity.UpdatedAt = entityDB.UpdatedAt
		entity.SoftLockDeletedAt = nil
		result := db.Save(&entity)
		return entity, result.Error
	} else {
		// Create
		log.Debug(fmt.Sprintf("IncomeExpenseNotionUpdateOrCreate: entity create: %+v", entity))
		result := db.Create(entity)
		return entity, result.Error
	}
}
