package configurator

import (
	"nomas_metabase_dashboard/pkg/repository/database"
	"nomas_shared/pkg/notion_utils"
)

type ServerConf struct {
	Port string `json:"port"`
}

type MetabaseDashboardServerConf struct {
	Debug    bool                       `json:"debug"`
	Server   ServerConf                 `json:"server"`
	Database database.DatabaseConf      `json:"database"`
	Notion   notion_utils.NotionApiConf `json:"notion"`
}
