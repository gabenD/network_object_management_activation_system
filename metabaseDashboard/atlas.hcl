data "external_schema" "gorm" {
  program = [
    "go",
    "run",
    "./loader",
  ]
}

env "gorm" {
  src = data.external_schema.gorm.url
  dev = "postgres://postgres:postgres@0.0.0.0:5432/metabase_dashboard_db?&sslmode=disable"
  migration {
    dir = "file://migrations"
  }
  format {
    migrate {
      diff = "{{ sql . \"  \" }}"
    }
  }
}
