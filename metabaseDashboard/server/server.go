package server

import (
	"encoding/json"
	"errors"
	"fmt"
	"nomas_metabase_dashboard/pkg/useCase"
	"time"

	"github.com/gofiber/fiber/v2"

	log "github.com/sirupsen/logrus"
)

func NewFiberServer(dependency *Dependency) func() {
	config := fiber.Config{
		ErrorHandler: func(ctx *fiber.Ctx, err error) error {
			log.Error(err)
			return ctx.Status(fiber.StatusInternalServerError).JSON(errors.New("panic error"))
		},
		IdleTimeout: 10 * time.Second,
	}
	app := fiber.New(config)
	dependency.FiberApp = app
	setRouter(dependency)
	return func() {
		port := fmt.Sprintf(":%s", dependency.ServerConfig.Port)
		err := app.Listen(port)
		if err != nil {
			log.Error("Can't start server due the error: %s\n", err.Error())
		}
	}
}

func setRouter(dependency *Dependency) {
	dependency.FiberApp.Post("/sync/notion", func(c *fiber.Ctx) error {
		var inputDTO useCase.NotionIncomeExpenseSyncToDBInputDTO
		err := json.Unmarshal(c.Body(), &inputDTO)
		if err != nil {
			return fiber.NewError(fiber.StatusBadRequest, "Invalid JSON format")
		}
		log.Debug(fmt.Sprintf("Запрос на метод /sync/notion с контентом %+v", inputDTO))
		output := dependency.NotionIncomeExpenseSyncToDbUC().Execute(inputDTO)
		return c.JSON(output)
	})
	dependency.FiberApp.Post("/sync/notion/limit", func(c *fiber.Ctx) error {
		var inputDTO useCase.NotionCurrencyLimitSyncToDBUCInputDTO
		err := json.Unmarshal(c.Body(), &inputDTO)
		if err != nil {
			return fiber.NewError(fiber.StatusBadRequest, "Invalid JSON format")
		}
		log.Debug(fmt.Sprintf("Запрос на метод /sync/notion/limit с контентом %+v", inputDTO))
		output := dependency.NotionCurrencyLimitSyncToDbUC().Execute(inputDTO)
		return c.JSON(output)
	})
}
