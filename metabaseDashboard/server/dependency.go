package server

import (
	"nomas_metabase_dashboard/pkg/configurator"
	"nomas_metabase_dashboard/pkg/repository/database"
	"nomas_metabase_dashboard/pkg/useCase"

	"github.com/dstotijn/go-notion"
	"github.com/gofiber/fiber/v2"
)

type Dependency struct {
	ServerConfig *configurator.ServerConf
	Debug        *bool
	Database     database.Connect
	conf         *configurator.MetabaseDashboardServerConf
	FiberApp     *fiber.App
}

func NewDependency(conf *configurator.MetabaseDashboardServerConf) *Dependency {
	db := database.Connect{Database: conf.Database}
	return &Dependency{
		ServerConfig: &configurator.ServerConf{
			Port: conf.Server.Port,
		},
		Debug:    &conf.Debug,
		Database: db,
		conf:     conf,
	}
}

func (c *Dependency) NotionIncomeExpenseSyncToDbUC() *useCase.NotionIncomeExpenseSyncToDBUC {
	return &useCase.NotionIncomeExpenseSyncToDBUC{
		NotionClient: notion.NewClient(c.conf.Notion.Token),
		Conf:         &c.conf.Notion,
		DBConnect:    &c.Database,
	}
}

func (c *Dependency) NotionCurrencyLimitSyncToDbUC() *useCase.NotionCurrencyLimitSyncToDBUC {
	return &useCase.NotionCurrencyLimitSyncToDBUC{
		NotionClient: notion.NewClient(c.conf.Notion.Token),
		Conf:         &c.conf.Notion,
		DBConnect:    &c.Database,
	}
}
